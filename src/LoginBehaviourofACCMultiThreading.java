import java.io.File;
import java.io.InputStream;
import java.lang.Thread.State;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Utils;
import appModule.Configuration;

import com.gallop.acc.org.AccMultiExecutor;
import com.relevantcodes.extentreports.ExtentTest;

public class LoginBehaviourofACCMultiThreading {

	// private static Logger Log = Logger.getLogger(LogIn.class.getName());
	// static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	// private static Reporter reporter = null;
	// static Properties properties = loadProperties();
	//private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");
	private static String chromePath = Utils.loadProperty("chromePath");
	private static String ieDriver = Utils.loadProperty("ieDriver");

	//public static String geckoDriverPath = "C:\\Pradeep Srivastava Backup\\Pradeep Backup\\geckodriver-v0.11.1-win64\\geckodriver.exe";
	static WebDriver dRDriver = null;
	static WebDriver driverFF = null;

	static WebDriver driverChrome = null;
	static WebDriver driverInternet = null;
	static DesiredCapabilities capabilities = null;
	static DesiredCapabilities capabilities1 = null;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		System.out.println("Main metho Starts for execution");

		String path="";
		
		path=new File(".").getCanonicalPath();
		
		
		//reportingPath= path+""+reportingPath;
		reportPath= path+""+reportPath;
		String destFilePath = null;
		com.gallop.Logger logger = new com.gallop.Logger();
		logger.createTestReport(reportPath + "\\testReports.html"/* "Q:\\QA Stuff\\Syam pithani\\NCDR\\NCDR\\WebContent\\WEB-INF\\views\\testReports.html" */);
		ExtentTest et = null;
		List<Thread> threadsList = new ArrayList<Thread>();
		ThreadGroup tg = new ThreadGroup("ACC");
		List<String> scenarios = new ArrayList<>();
		scenarios.add("VerifyingBasicLogin");
		scenarios.add("VerifyingBasicSSOConnection");
		scenarios.add("VerifyRememberMeDoesNotBreakAccesstoLLPProducts");
		scenarios.add("VerifyRememberMeFunctionlityonACC");
		scenarios.add("VerifyRememberMeFunctionalityIfUserVisitsLLP");
		scenarios.add("VerifyRememberMeFunctionalityonQII");
		scenarios.add("VerifyRememberMeFunctionalityonScientificSession");
		Configuration configuration = new Configuration();

		configuration.setBrowser("ie");

		ExecutorService executor = Executors.newFixedThreadPool(5);
		Long timeStart = System.currentTimeMillis();
		if (scenarios != null && !scenarios.isEmpty()) {
			for (String scenario : scenarios) {
				Runnable exec = new AccMultiExecutor(dRDriver, logger,
						scenario, tg, threadsList, configuration);
				executor.execute(exec);
				/* logger.flush(); */
			}
		}
		executor.shutdown();
		Long time = System.currentTimeMillis();
		/*for (Thread thread : threadsList) {
			System.out.println("Joinig Thread name:" + thread.getName());
			System.out.println("Thread name:" + thread.getName()
					+ " isAlive:" + thread.isAlive() + " State:"
					+ thread.getState());
			thread.join(60000);
		}*/
		Boolean allDead = true;
		time = System.currentTimeMillis();
		do {

			try {
				for (Thread thread : threadsList) {
					allDead = true;
					if (!(thread.getState() == State.TERMINATED)
							&& thread.isAlive()) {
						/*System.out
								.println("Making thread sleep for 60000 msecs");*/
						/*Thread.sleep(15000);*/
						allDead = false;
						break;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		} while (!allDead);
		destFilePath = Utils.createReportWithTimeStamp(configuration
				.getBrowser());

	}

}
