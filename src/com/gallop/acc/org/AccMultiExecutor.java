package com.gallop.acc.org;


import java.io.File;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import package1.LoginBehaviourofACC;
import utility.Reporter;
import utility.Utils;
import appModule.Configuration;
import beans.Registry;

import com.relevantcodes.extentreports.ExtentTest;

public class AccMultiExecutor implements Runnable {

	Configuration configuration;
	WebDriver driver;
	com.gallop.Logger logger;
	String scenario;
	ThreadGroup tg;
	List<Thread> threadList;
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");

	public AccMultiExecutor(WebDriver driver, com.gallop.Logger logger,
			String scenario, ThreadGroup tg, List<Thread> threadList,
			Configuration configuration) {
		this.driver = driver;
		this.logger = logger;
		this.scenario = scenario;
		this.tg = tg;
		this.threadList = threadList;
		this.configuration = configuration;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		/* synchronized (configuration) { */

		long timeStart = System.currentTimeMillis();

		processThread();
		long timeEnd = System.currentTimeMillis();
		System.out.println("Time taken for thread: " + (timeEnd - timeStart)
				+ " millis");
		/* } */

	}

	private void processThread() {
		try {
			String path="";
			
			path=new File(".").getCanonicalPath();
			
			
			reportingPath= path+""+reportingPath;
			reportPath= path+""+reportPath;
			Thread.currentThread().setName(scenario);
			threadList.add(Thread.currentThread());
			System.out.println("Thread " + Thread.currentThread().getName()
					+ " added while scenario:" + scenario);
			DesiredCapabilities capabilities = null;
			String run = configuration.getBrowser();
			WebDriver driver1=null;
			try {
				if (run.trim().equalsIgnoreCase("ie")) {
					// Open IE
					driver1 = LoginBehaviourofACC.OpenIEBrowser(driver,
							capabilities);
				}

				else if (run.trim().equalsIgnoreCase("firefox"))
				// Open Firefox
				{
					driver1 = LoginBehaviourofACC.OpenFFBrowser(driver,
							capabilities);
				} else if (run.trim().equalsIgnoreCase("Chrome"))
				// Open Chrome
				{
					driver1 = LoginBehaviourofACC.OpenChrmBrowser(driver,
							capabilities);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ExtentTest et = null;
			driver1.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
			ScenarioForAcc.page_Wait();

			List<WebElement> loginPagePopUP = driver1.findElements(By
					.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
			ScenarioForAcc.page_Wait();
			if (loginPagePopUP.size() > 0) {
				loginPagePopUP.get(0).click();
				ScenarioForAcc.page_Wait();
			}
			if (scenario.equalsIgnoreCase("VerifyingBasicLogin")) {
				LoginBehaviourofACC.VerifyingBasicLogin(reportPath, driver1,
						logger, reportingPath, et);
				ScenarioForAcc.page_Wait();
			}

			if (scenario.equalsIgnoreCase("VerifyingBasicSSOConnection")) {
				// System.out.println("scenario2 started");
				LoginBehaviourofACC.VerifyingBasicSSOConnection(reportPath,
						driver1, logger, reportingPath, et);
				ScenarioForAcc.page_Wait();
			}

			if (scenario
					.equalsIgnoreCase("VerifyRememberMeDoesNotBreakAccesstoLLPProducts")) {
				// System.out.println("scenario3 started");
				driver1 = LoginBehaviourofACC
						.VerifyRememberMeDoesNotBreakAccesstoLLPProducts(
								reportPath, driver1, logger, reportingPath, et);
				ScenarioForAcc.page_Wait();
			}

			if (scenario.equalsIgnoreCase("VerifyRememberMeFunctionlityonACC")) {
				// System.out.println("scenario4 started");
				driver1 = LoginBehaviourofACC.VerifyRememberMeFunctionlityonACC(
						reportPath, driver1, logger, reportingPath, et);
				ScenarioForAcc.page_Wait();
			}

			if (scenario
					.equalsIgnoreCase("VerifyRememberMeFunctionalityIfUserVisitsLLP")) {
				System.out.println("scenario5 started");
				driver1 = LoginBehaviourofACC
						.VerifyRememberMeFunctionalityIfUserVisitsLLP(
								reportPath, driver1, logger, reportingPath, et);
				ScenarioForAcc.page_Wait();
			}

			if (scenario.equalsIgnoreCase("VerifyRememberMeFunctionalityonQII")) {
				// System.out.println("scenario6 started");
				driver1 = LoginBehaviourofACC
						.VerifyRememberMeFunctionalityonQII(reportPath, driver1,
								logger, reportingPath, et);
				ScenarioForAcc.page_Wait();
			}

			if (scenario
					.equalsIgnoreCase("VerifyRememberMeFunctionalityonScientificSession")) {
				// System.out.println("scenario7 started");
				driver1 = LoginBehaviourofACC
						.VerifyRememberMeFunctionalityonScientificSession(
								reportPath, driver1, logger, reportingPath, et);
				ScenarioForAcc.page_Wait();
			}
			Utils.close_Browser(driver1);
		} catch (Exception e) {
			// throw new RuntimeException();
			e.printStackTrace();
		}
	}

}
