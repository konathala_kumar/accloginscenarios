package package1;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Utils;

import com.gallop.acc.org.ScenarioForAcc;
import com.gargoylesoftware.htmlunit.javascript.host.media.webkitMediaStream;
import com.relevantcodes.extentreports.ExtentTest;

public class LoginBehaviourofACC {

	// private static Logger Log = Logger.getLogger(LogIn.class.getName());
	// static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	// private static Reporter reporter = null;
	// static Properties properties = loadProperties();
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");
	private static String chromePath = Utils.loadProperty("chromePath");
	private static String ieDriver = Utils.loadProperty("ieDriver");

	//public static String geckoDriverPath = "C:\\Pradeep Srivastava Backup\\Pradeep Backup\\geckodriver-v0.11.1-win64\\geckodriver.exe";
	static WebDriver dRDriver = null;
	static WebDriver driverFF = null;

	static WebDriver driverChrome = null;
	static WebDriver driverInternet = null;
	static DesiredCapabilities capabilities = null;
	static DesiredCapabilities capabilities1 = null;
	static WebElement element=null;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		System.out.println("Main metho Starts for execution");

		String path="";
		
		path=new File(".").getCanonicalPath();
		
		
		reportingPath= path+""+reportingPath;
		reportPath= path+""+reportPath;
		
		String destFilePath = null;
		com.gallop.Logger logger = new com.gallop.Logger();
		logger.createTestReport(reportPath + "\\testReports.html"/* "Q:\\QA Stuff\\Syam pithani\\NCDR\\NCDR\\WebContent\\WEB-INF\\views\\testReports.html" */);

		ExtentTest et = null;

//	dRDriver = OpenChrmBrowser(dRDriver, capabilities);
	dRDriver =OpenGeckoBrowser(dRDriver, capabilities);
//		 dRDriver = OpenFFBrowser(dRDriver, capabilities);
//		dRDriver = OpenIEBrowser(dRDriver, capabilities);

		// dRDriver = OpenFirefoxBrowser(dRDriver);

		// dRDriver=OpenIEBrowser(dRDriver);

		dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
		
		//Ramya
		Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();
		if (browserName.equalsIgnoreCase("firefox")
							|| browserName.equalsIgnoreCase("chrome")) 
		{

		ScenarioForAcc.page_Wait();
		}
		else if(browserName.equalsIgnoreCase("ie"))
		{
			dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}


		List<WebElement> loginPagePopUP = dRDriver.findElements(By
				.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
		
		/*if (browserName.equalsIgnoreCase("firefox")
							|| browserName.equalsIgnoreCase("chrome")) 
		{

		ScenarioForAcc.page_Wait();
		}
		else if(browserName.equalsIgnoreCase("ie"))
		{
			dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}*/
		//ScenarioForAcc.page_Wait();
		if (loginPagePopUP.size() > 0) {
			loginPagePopUP.get(0).click();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			ScenarioForAcc.page_Wait();
		}

	
		VerifyingBasicLogin(reportPath, dRDriver, logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		
	
		VerifyingBasicSSOConnection(reportPath, dRDriver, logger,
				reportingPath, et);
			ScenarioForAcc.page_Wait1();
			
		dRDriver = VerifyRememberMeDoesNotBreakAccesstoLLPProducts(reportPath,
				dRDriver, logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		
		dRDriver = VerifyRememberMeFunctionlityonACC(reportPath, dRDriver,
				logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		dRDriver = VerifyRememberMeFunctionalityIfUserVisitsLLP(reportPath,
				dRDriver, logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		dRDriver = VerifyRememberMeFunctionalityonQII(reportPath, dRDriver,
				logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		
		dRDriver = VerifyRememberMeFunctionalityonScientificSession(reportPath,
				dRDriver, logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
	
	
		dRDriver.close();
		// String url=DR_ACCMain.run;
		String run = null;

		destFilePath = Utils.createReportWithTimeStamp(run);

	}

	public static WebDriver OpenFFBrowser(WebDriver dRDriver,
			DesiredCapabilities capabilities) throws Exception {
		// System.setProperty("webdriver.gecko.driver", geckoDriverPath);

		capabilities = DesiredCapabilities.firefox();
		FirefoxProfile profile = new FirefoxProfile();

		capabilities.setCapability(FirefoxDriver.PROFILE, profile);

		dRDriver = new FirefoxDriver();
		dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		dRDriver.manage().window().maximize();

		driverFF = dRDriver;

		return dRDriver;
	}

	public static WebDriver OpenIEBrowser(WebDriver dRDriver,
			DesiredCapabilities capabilities) throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		// String url = conf.getUrl();// ExcelUtils.getCellData(2, 1);
		//********************************
		System.setProperty("webdriver.ie.driver", /* Constant.Path_TestData */
				ieDriver + "IEDriverServer_64.exe");
		dRDriver = new InternetExplorerDriver();
		
		//********************************
		
		//--------------------------------------------------------
		/*System.setProperty("webdriver.ie.driver",  Constant.Path_TestData 
				ieDriver + "IEDriverServer_53.1.exe");
		DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
		//commented
		ieCapabilities
				.setCapability(
						InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);

		//ieCapabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		//ieCapabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
		ieCapabilities.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
		//ieCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, false);

		ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCapabilities.setCapability("nativeEvents", false);
		ieCapabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
		dRDriver = new InternetExplorerDriver(ieCapabilities);*/
		//----------------------------------------------------------------
		//commented
		/*
		 * InternetExplorerDriverService.Builder builder = new
		 * InternetExplorerDriverService.Builder();
		 * InternetExplorerDriverService srvc = builder.usingPort(5555)
		 * .withHost("127.0.0.1").build();
		 */

		// driver = new InternetExplorerDriver(srvc, ieCapabilities);
	
		//dRDriver = new InternetExplorerDriver();
		dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// dRDriver.get(url);
//		dRDriver.manage().window().maximize();

		driverInternet = dRDriver;
		return dRDriver;
	}

	public static WebDriver OpenChrmBrowser(WebDriver dRDriver,
			DesiredCapabilities capabilities) throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		// String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);

		// dRDriver.get(url);

		System.setProperty("webdriver.chrome.driver", chromePath
				+ "chromedriver.exe");
		/*
		 * capabilities = DesiredCapabilities.chrome(); ChromeOptions options =
		 * new ChromeOptions(); options.addArguments("test-type");
		 * options.addArguments("start-maximized");
		 * options.addArguments("user-data-dir=" + chromePath);
		 * capabilities.setCapability("chrome.binary", chromePath +
		 * "chromedriver.exe");
		 * capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		 */

		// capabilities=getCapabilities(capabilities);
		// dRDriver = new ChromeDriver(capabilities);
		dRDriver = new ChromeDriver();

//		 dRDriver.manage().window().maximize();
		driverChrome = dRDriver;

		/*
		 * dRDriver = new ChromeDriver();
		 */
		return dRDriver;
	}
	public static WebDriver OpenGeckoBrowser(WebDriver dRDriver,
			DesiredCapabilities capabilities) throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		// String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);

		// dRDriver.get(url);

		System.setProperty("webdriver.gecko.driver","C:\\Users\\E002493\\Desktop\\offshore\\downloads\\geckodriver.exe");
        
        // if above property is not working or not opening the application in browser then try below property

       //System.setProperty("webdriver.firefox.marionette","G:\\Selenium\\Firefox driver\\geckodriver.exe");

       dRDriver = new FirefoxDriver();
		

		/*
		 * dRDriver = new ChromeDriver();
		 */
		return dRDriver;
	}

	public static WebDriver OpenFirefoxBrowser(WebDriver dRDriver)
			throws Exception {

		/*
		 * capabilities = DesiredCapabilities.firefox(); FirefoxProfile options
		 * = new FirefoxProfile();
		 * 
		 * options.setPreference("user-data-dir" , reportingPath);
		 * capabilities.setCapability(FirefoxDriver.PROFILE, options);
		 * 
		 * 
		 * dRDriver = new FirefoxDriver(options);
		 */
		/*
		 * capabilities=getCapabilitiesForFF(capabilities);
		 * 
		 * 
		 * 
		 * dRDriver = new FirefoxDriver(capabilities);
		 * 
		 * driverFF=dRDriver;
		 */
		ProfilesIni profile = new ProfilesIni();

		FirefoxProfile myprofile = profile.getProfile("Testfirefox");
		// myprofile.setPreference("network.cookie.cookieBehavior", 0);
		// myprofile.setPreference("browser.cache.memory.enable",true);
		dRDriver = new FirefoxDriver(myprofile);

		/*
		 * dRDriver = new ChromeDriver();
		 */
		return dRDriver;
	}

	public static DesiredCapabilities getCapabilities(
			DesiredCapabilities capabilities) throws Exception {

		/*
		 * * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */

		// String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);

		// dRDriver.get(url);

		if (capabilities != null) {
			return capabilities;
		}

		/*
		 * System.setProperty("webdriver.chrome.driver", reportingPath +
		 * "chromedriver.exe"); capabilities = DesiredCapabilities.chrome();
		 * ChromeOptions options = new ChromeOptions();
		 * options.addArguments("test-type");
		 * options.addArguments("start-maximized");
		 * options.addArguments("user-data-dir=" + reportingPath);
		 * capabilities.setCapability("chrome.binary", reportingPath +
		 * "chromedriver.exe");
		 * capabilities.setCapability(ChromeOptions.CAPABILITY, options); return
		 * capabilities;
		 * 
		 * 
		 * 
		 * // System.setProperty("webdriver.chrome.driver", /*
		 * Constant.Path_TestData /*reportingPath + "chromedriver.exe");
		 * 
		 * FirefoxProfile profile = new FirefoxProfile();
		 * 
		 * 
		 * DesiredCapabilities dc=DesiredCapabilities.firefox(); FirefoxProfile
		 * profile1 = new FirefoxProfile();
		 * dc.setCapability(FirefoxDriver.PROFILE, profile); return dc;
		 * 
		 * 
		 * capabilities = DesiredCapabilities.firefox(); Firefox options = new
		 * ChromeOptions(); options.addArguments("test-type");
		 * options.addArguments("start-maximized");
		 * options.addArguments("user-data-dir=" + reportingPath);
		 * capabilities.setCapability("chrome.binary", reportingPath +
		 * "chromedriver.exe");
		 * capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		 */
		/*
		 * FirefoxProfile profile = new FirefoxProfile();
		 * 
		 * 
		 * DesiredCapabilities dc=DesiredCapabilities.firefox(); FirefoxProfile
		 * profile1 = new FirefoxProfile();
		 * dc.setCapability(FirefoxDriver.PROFILE, profile1); return dc;
		 */

		/*
		 * capabilities = DesiredCapabilities.firefox(); Firefox options = new
		 * ChromeOptions(); options.addArguments("test-type");
		 * options.addArguments("start-maximized");
		 * options.addArguments("user-data-dir=" + reportingPath);
		 * capabilities.setCapability("chrome.binary", reportingPath +
		 * "chromedriver.exe");
		 * capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		 */
		return capabilities;
	}

	public static DesiredCapabilities getCapabilitiesForFF(
			DesiredCapabilities capabilities) throws Exception {

		if (capabilities != null) {
			return capabilities;
		}

		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.cache.disk.enable", true);
		profile.setPreference("browser.cache.memory.enable", true);
		profile.setPreference("browser.cache.offline.enable", true);
		profile.setPreference("network.http.use-cache", true);

		DesiredCapabilities dc = DesiredCapabilities.firefox();
		dc.setCapability(FirefoxDriver.PROFILE, profile);
		return capabilities;

	}

	public static void BasicLoginFeature(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			/*
			 * ScenarioForAcc.page_Wait(); Thread.sleep(5000);
			 */
			CapturingPopup(dRDriver);
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//ScenarioForAcc.page_Wait();
			// ScenarioForAcc.page_Wait();
			try {
				WebElement action1 = dRDriver.findElement(By
						.xpath("//*[@id='myacc-holder']/div/div[2]/p/a")); // login
																			// button
								Thread.sleep(5000);											// validation
				if (action1.isDisplayed()) {
					System.out.println("Element displayed");
					dRDriver.findElement(By
							.xpath("//*[@id='myacc-holder']/div/div[2]/p/a")).click();
					Thread.sleep(5000);
					Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
					String browserName = cap.getBrowserName().toLowerCase();
					if (browserName.equalsIgnoreCase("firefox")
							|| browserName.equalsIgnoreCase("chrome")) 
					{

						ScenarioForAcc.page_Wait();
					}
					else if(browserName.equalsIgnoreCase("ie"))
					{
						dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					}
					logger.log("pass", "login button exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {

				// e.printStackTrace();
				logger.log("fail", "Login button is not clicked" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			// ScenarioForAcc.page_Wait();

			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			// ScenarioForAcc.page_Wait();
			try {
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{

					ScenarioForAcc.page_Wait1();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}

				WebDriverWait wait=new WebDriverWait(dRDriver, 10);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='lvl2-masthead']/h1")));
				String login = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

				if (login.contains("Login")) {

					logger.log("pass", "Login page displayed for acc", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			// ScenarioForAcc.page_Wait();
			System.out.println("login screen captured");

			try {

				/*
				 * String UserNameForMembership = properties
				 * .getProperty("UserNameForMembership");
				 */
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
						UserNameForMembership); // this enters the username

				String username = dRDriver.findElement(
						By.xpath("//*[@id='UserName']")).getAttribute("value");

				System.out.println(username);
				// ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName " + '"' + username + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			// ScenarioForAcc.page_Wait();

			try {

				// dRDriver.findElement(By.id("Password")).click(); //this will
				// click on the password field
				//ScenarioForAcc.page_Wait();
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				/*if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}*/
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.id("Password")).sendKeys(
						PasswordForMembership); // this will enter the password
												// data
				String password = dRDriver.findElement(By.id("Password"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password " + '"' + password + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			/*if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}*/
			// et=logger.createTest("password", "password entered");
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
			 * .click(); ScenarioForAcc.page_Wait();
			 */

			try {

				WebElement signIn = dRDriver.findElement(By
						.xpath(".//*[@id='formLogin']/div[3]/button"));
				Thread.sleep(5000);
				// check remember me check
				String signInText = dRDriver.findElement(
						By.xpath(".//*[@id='formLogin']/div[3]/button")).getText();
				Thread.sleep(5000);
				Thread.sleep(5000);
				
				if (signIn.isDisplayed()) {  
					signIn.click();
					/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
					String browserName = cap.getBrowserName().toLowerCase();*/
					if (browserName.equalsIgnoreCase("firefox")
							|| browserName.equalsIgnoreCase("chrome")) 
					{
						Thread.sleep(20000);
						/*ScenarioForAcc.page_Wait();
						ScenarioForAcc.page_Wait();
						Thread.sleep(5000);Thread.sleep(5000);
						Thread.sleep(5000);*/
					}
					else if(browserName.equalsIgnoreCase("ie"))
					{
						dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					}
					logger.log("pass", "Clicked on" + '"' + signInText + '"'
							+ "button", false, dRDriver, reportingPath, et);

				}
				System.out.println("Successfully logged in to ACC site");
				// dRDriver.manage().timeouts().implicitlyWait(10,
				// TimeUnit.SECONDS);
				// ScenarioForAcc.page_Wait();
				/*if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{

				WebDriverWait wait = new WebDriverWait(dRDriver, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span")));
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
*/Thread.sleep(5000);
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//ScenarioForAcc.page_Wait();

			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);
				if (logoutLink.contains("Log Out")) {
					logger.log("pass", "The" + '"' + logoutLink + '"'
							+ " Link is displayed", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

				// dRDriver.manage().timeouts().implicitlyWait(5,
				// TimeUnit.SECONDS);
				// ScenarioForAcc.page_Wait();
			}
			logger.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void BasicLoginFeatureForEchoSAP7(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			String urlForEchoSap7 = "http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx";
			if (!urlForEchoSap7.isEmpty()) {
				dRDriver.get(urlForEchoSap7);
				
			}
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			logger.log("pass", "Accessed Echosap7 page", false, dRDriver,
					reportingPath, et);
			try {
				WebElement Login = dRDriver
						.findElement(By
								.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"));

				if (Login.isDisplayed()) {
					
					/*if (browserName.equalsIgnoreCase("firefox")
										|| browserName.equalsIgnoreCase("chrome")) 
					{

					ScenarioForAcc.page_Wait();
					}
					else if(browserName.equalsIgnoreCase("ie"))
					{
						dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					}*/
					//ScenarioForAcc.page_Wait();
					logger.log("pass", "Home page displayed for Echosap7",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Home page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				logger.log("fail", "Home page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );
			// ScenarioForAcc.page_Wait();
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.click();
				ScenarioForAcc.page_Wait();
				Thread.sleep(5000);
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}

				String login = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

				Thread.sleep(5000);
				if (login.contains("Login")) {
					logger.log("pass", "Login page displayed for Echosap7",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//ScenarioForAcc.page_Wait();
			System.out.println("login screen captured");

			try {
				// dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
				// ScenarioForAcc.page_Wait();
				// driver.findElement(By.id("UserName")).clear();

				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
						UserNameForMembership);

				String username = dRDriver.findElement(
						By.xpath("//*[@id='UserName']")).getAttribute("value");

				System.out.println(username);
				// ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName " + '"' + username + '"'
							+ " successfully entered for Echosap7", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//ScenarioForAcc.page_Wait();

			try {
				// //dRDriver.findElement(By.id("Password")).click();
				// ScenarioForAcc.page_Wait();
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.id("Password")).sendKeys(
						PasswordForMembership);
				String password = dRDriver.findElement(By.id("Password"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password " + '"' + password + '"'
							+ " successfully entered for Echosap7", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// et=logger.createTest("password", "password entered");
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
			 * .click(); ScenarioForAcc.page_Wait();
			 */

			// et=logger.createTest("Login", "Login successful");

			// System.out.println("Successfully logged in to ACC site");
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			// ScenarioForAcc.page_Wait();

			// page_Wait();

			// CapturingPopup(dRDriver);
			// dRDriver.manage().timeouts().implicitlyWait(10,
			// TimeUnit.SECONDS);

			try {
				WebElement loginClick = dRDriver.findElement(By
						.xpath(".//*[@id='formLogin']/div[3]/button"));
				if (loginClick.isDisplayed()) {
					loginClick.click();
					Thread.sleep(15000);
					try {
						List<WebElement> loginPagePopUP = dRDriver.findElements(By
								.xpath(".//*[@id='acsMainInvite']/div/a[1]"));
						//ScenarioForAcc.page_Wait();
						
						if (browserName.equalsIgnoreCase("firefox")
											|| browserName.equalsIgnoreCase("chrome")) 
						{

						ScenarioForAcc.page_Wait();
						}
						else if(browserName.equalsIgnoreCase("ie"))
						{
							dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						}
						if (loginPagePopUP.size() > 0) {
							loginPagePopUP.get(0).click();
						
							if (browserName.equalsIgnoreCase("firefox")
												|| browserName.equalsIgnoreCase("chrome")) 
							{

							ScenarioForAcc.page_Wait();
							}
							else if(browserName.equalsIgnoreCase("ie"))
							{
								dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							}
							//ScenarioForAcc.page_Wait();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
					String browserName = cap.getBrowserName().toLowerCase();*/
					if (browserName.equalsIgnoreCase("firefox")
										|| browserName.equalsIgnoreCase("chrome")) 
					{

					/*	WebDriverWait wait4 = new WebDriverWait(dRDriver, 20);
						wait4.until(ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@id='nav-myacc']/li[1]/span")))*/
						Thread.sleep(5000);
					}
					else if(browserName.equalsIgnoreCase("ie"))
					{
						dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					}
					
					//ScenarioForAcc.page_Wait();
					logger.log("pass", "Clicked on Login button", false,
							dRDriver, reportingPath, et);
				}

				//Thread.sleep(300);
				element=dRDriver.findElement(By.xpath("//*[@id='nav-myacc']/li[1]/span"));
				boolean ele = waitForURLToMatch(element, 20);
				if(ele==true)
				{
					
				
				WebElement logoforacc = dRDriver.findElement(By
						.xpath("//*[@id='nav-myacc']/li[1]/span"));
				Thread.sleep(5000);
			
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass",
							"Successfully logged into ACC for Echosap7", false,
							dRDriver, reportingPath, et);
				}
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();

				if (logoutLink.contains("Log Out")) {
					logger.log("pass", "The" + '"' + logoutLink + '"'
							+ " Link is displayed", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
		} // try ending
		catch (Exception e) {
			e.printStackTrace();
		}

	}// method ending

	public static void CapturingPopup(WebDriver dRDriver) {
		try {
			List<WebElement> popUpElements = dRDriver
					.findElements(By
							.xpath("//*[@id='fsrOverlay']/div/div/div/div/div/div[2]/div[1]/a"));
			if (popUpElements.size() > 0) {
				popUpElements.get(0).click();
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				//ScenarioForAcc.page_Wait();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void LogoutFeature(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {
			try
			{
				dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
				//ScenarioForAcc.page_Wait();
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				

				if (browserName.equalsIgnoreCase("ie")) {
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				else
				{
					/*Thread.sleep(5000);
					Thread.sleep(5000);*/
					ScenarioForAcc.page_Wait();
				}
				
				WebDriverWait wait = new WebDriverWait(dRDriver, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.id("header-personal")));
				
				logger.log("pass","Navigated to acc again after disclosure",false, dRDriver, reportingPath, et);
			}
			catch(Exception e)
			{
				logger.log("fail","not navigated to acc after disclosure",true, dRDriver, reportingPath, et);
			}
			logger.flush();
			//dRDriver.navigate().refresh();
			
			System.out.println("next this will execute the logout functionality");
			//System.out.println(dRDriver.getCurrentUrl());
			try {
				//System.out.println("in logout!!");
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				CapturingPopUp(dRDriver, browserName);
				WebElement ele = dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a"));
				Thread.sleep(5000);
				/*Thread.sleep(5000);	
				Thread.sleep(5000);	*/
				System.out.println("this is for testing the logout text " + ele.getText());
				if(ele.isDisplayed())
				{
					dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).click();
					Thread.sleep(15000);
				/*	WebDriverWait wait =new WebDriverWait(dRDriver, 10);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")));*/
//					Thread.sleep(10000);
					/*Thread.sleep(5000);
					Thread.sleep(5000);	
					Thread.sleep(5000);

					Thread.sleep(5000);*/
					ScenarioForAcc.page_Wait();
					CapturingPopUp(dRDriver, browserName);
					if (browserName.equalsIgnoreCase("firefox")
							
										|| browserName.equalsIgnoreCase("chrome")) 
					{

					ScenarioForAcc.page_Wait();
					}
					else if(browserName.equalsIgnoreCase("ie"))
					{
						dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					}
					Thread.sleep(5000);
				
				logger.log("pass","logout link exists and successfully clicked on logout link",false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail","logout link does not exists ",true, dRDriver, reportingPath, et);
				}
				/*JavascriptExecutor executor = (JavascriptExecutor) dRDriver;
				executor.executeScript("arguments[0].click();", ele);*/
				
				System.out.println("After Clicking logout!!");
//CapturingPopup(dRDriver);
				// ScenarioForAcc.page_Wait();
				//System.out.println(dRDriver.getCurrentUrl());

				WebDriverWait wait=new WebDriverWait(dRDriver, 10);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")));
				
				WebElement element = dRDriver.findElement(By
						.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"));
				
				Thread.sleep(5000);
				
				
				if (element.isDisplayed()) {
					logger.log(
							"pass",
							"logged out  successfully and home page for ACC is displayed",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "log out is not successful" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "log out is not successful" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void LogoutFeatureCVQuality(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
	try {
		try
		{
			dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
			ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();

			if (browserName.equalsIgnoreCase("ie")) {
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			else
			{
			
				ScenarioForAcc.page_Wait();
			}
			
			/*WebDriverWait wait = new WebDriverWait(dRDriver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("header-personal")));*/
			
			logger.log("pass","Navigated to acc again after disclosure",false, dRDriver, reportingPath, et);
		}
		catch(Exception e)
		{
			logger.log("fail","not navigated to acc after disclosure",true, dRDriver, reportingPath, et);
		}
		logger.flush();
		//dRDriver.navigate().refresh();
		
		System.out.println("next this will execute the logout functionality");
		//System.out.println(dRDriver.getCurrentUrl());
		try {
			//System.out.println("in logout!!");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			/*if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}*/
			//ScenarioForAcc.page_Wait();
			WebElement ele = dRDriver.findElement(By.xpath(".//*[@id='TopNavigation_TF9FBB7A2001_ctl00_ctl00_navigationUl']/li[5]/a"));
			//Thread.sleep(3000);
			System.out.println("this is for testing the logout text " + ele.getText());
			if(ele.isDisplayed())
			{
				ele.click();
				ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
			logger.log("pass","logout link exists and successfully clicked on logout link",false, dRDriver, reportingPath, et);
			}
			else
			{
				logger.log("fail","logout link does not exists ",true, dRDriver, reportingPath, et);
			}
			/*JavascriptExecutor executor = (JavascriptExecutor) dRDriver;
			executor.executeScript("arguments[0].click();", ele);*/
			
			System.out.println("After Clicking logout!!");
       CapturingPopup(dRDriver);
			// ScenarioForAcc.page_Wait();
			//System.out.println(dRDriver.getCurrentUrl());
			WebElement Homepage = dRDriver.findElement(By
					.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"));
			Thread.sleep(5000);
			Thread.sleep(5000);
			if (Homepage.isDisplayed()) {
				logger.log(
						"pass",
						"logged out  successfully and home page for ACC is displayed",
						false, dRDriver, reportingPath, et);
			}
			else
			{
				logger.log("fail", "log out is not successful" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "log out is not successful" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);
		}
		logger.flush();

		List<WebElement> loginPagePopUP = dRDriver.findElements(By
				.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
		//ScenarioForAcc.page_Wait();
		Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();
		if (browserName.equalsIgnoreCase("firefox")
							|| browserName.equalsIgnoreCase("chrome")) 
		{

		ScenarioForAcc.page_Wait();
		}
		else if(browserName.equalsIgnoreCase("ie"))
		{
			dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
		if (loginPagePopUP.size() > 0) {
			loginPagePopUP.get(0).click();
		
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
		}

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	public static void LogoutFeatureForACCScientific(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			try {
				dRDriver.findElement(By.xpath(".//*[@id='logout_btn']"))
						.click();

				Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);
				ScenarioForAcc.page_Wait();
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}

				String LoginPage = dRDriver.findElement(
						By.xpath(".//*[@id='login_btn']")).getText();
				Thread.sleep(5000);
				Thread.sleep(5000);
				ScenarioForAcc.page_Wait();
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{
					ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				
				if (LoginPage.contains("Login")) {
					logger.log("pass",
							"Logged out successfully for ACCScentific and  "
									+ '"' + LoginPage + '"'
									+ " tab exixts ForACCScientific", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "ACCScientific is not logged out" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				// e2.printStackTrace();
				logger.log("fail", "ACCScientific is not logged out" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

		//	ScenarioForAcc.page_Wait();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void LogoutFeatureForJACCJournal(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']")).click();

				//ScenarioForAcc.page_Wait();
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				WebElement LogoForJACC = dRDriver.findElement(By
						.xpath(".//*[@id='siteLogo']"));

				if (LogoForJACC.isDisplayed()) {
					logger.log("pass",
							"Logged in successfully  ForJACCJournal", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Logged in not uccessfull  ForJACCJournal"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Logged in not uccessfull  ForJACCJournal"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void RememberMeFeature(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			/*
			 * et = logger.createTest("ACCMemberLoginWith Remember Me Feature",
			 * "Login Test"); Thread.sleep(4000);
			 */
			dRDriver.get("http://www.acc.org/");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
//			ScenarioForAcc.page_Wait();
			try {
				WebElement action1 = dRDriver.findElement(By
						.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
				Thread.sleep(5000);
				if (action1.isDisplayed()) {
					System.out.println("Element displayed");

					logger.log("pass",
							"Home page displayed and login button is clicked",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login button is not clicked" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {

				// e.printStackTrace();
				logger.log("fail", "Login button is not clicked" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			ScenarioForAcc.page_Wait();
			
			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );
			//ScenarioForAcc.page_Wait();
			try {
				CapturingPopUp(dRDriver, browserName);
				Thread.sleep(5000);
				dRDriver.findElement(
						By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"))
						.click();
				ScenarioForAcc.page_Wait();
				Thread.sleep(15000);
				/*Thread.sleep(5000);
				Thread.sleep(5000);*/
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}
				
				String login = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				Thread.sleep(5000);
				if (login.contains("Login")) {
					logger.log("pass", "Login page displayed for acc", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			//System.out.println("login screen captured");
			try {
				/*dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
				//ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}*/
				// driver.findElement(By.id("UserName")).clear();

				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
						UserNameForMembership);

				String username = dRDriver.findElement(
						By.xpath("//*[@id='UserName']")).getAttribute("value");

				System.out.println(username);
				//ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName" + '"' + username + '"'
							+ "successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			try {
				/*dRDriver.findElement(By.id("Password")).click();
				//ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}*/
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.id("Password")).sendKeys(
						PasswordForMembership);

				String password = dRDriver.findElement(By.id("Password"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password" + '"' + password + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			//ScenarioForAcc.page_Wait();

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='formLogin']/div[3]/p[3]/label")).click();

				//ScenarioForAcc.page_Wait();
				dRDriver.findElement(By.xpath(".//*[@id='formLogin']/div[3]/button"))
						.click();
				Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);
				// et=logger.createTest("Login", "Login successful");
				// dRDriver.manage().timeouts().implicitlyWait(10,
				// TimeUnit.SECONDS);
				//ScenarioForAcc.page_Wait();
			CapturingPopUp(dRDriver, browserName);
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				WebDriverWait wait = new WebDriverWait(dRDriver, 20);
				wait.until(ExpectedConditions.elementToBeClickable(By
						.xpath("//*[@id='nav-myacc']/li[1]/span")));
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}

				
				System.out.println("Successfully logged in to ACC site");

				// page_Wait();

				CapturingPopup(dRDriver);
			    
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				Thread.sleep(5000);
				Thread.sleep(5000);
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			//ScenarioForAcc.page_Wait();

			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();

				if (logoutLink.contains("Log Out")) {
					logger.log("pass", "The" + '"' + logoutLink + '"'
							+ " Link is displayed", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			} catch (Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			//ScenarioForAcc.page_Wait();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void RememberMeFeatureForNonMember(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			/*
			 * et = logger.createTest("ACCMemberLoginWith Remember Me Feature",
			 * "Login Test"); Thread.sleep(4000);
			 */
			dRDriver.get("http://www.acc.org/");
		//	ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}

			List<WebElement> loginPagePopUP = dRDriver.findElements(By
					.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
			// ScenarioForAcc.page_Wait();
			if (loginPagePopUP.size() > 0) {
				loginPagePopUP.get(0).click();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}
			//	ScenarioForAcc.page_Wait();
			}

			try {
				WebElement action1 = dRDriver.findElement(By
						.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
				Thread.sleep(5000);
				if (action1.isDisplayed()) {
					System.out.println("Element displayed");

					logger.log("pass", "action performed", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login button is not available" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {

				// e.printStackTrace();
				logger.log("fail", "Login button is not available" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			// ScenarioForAcc.page_Wait();

			/*System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");*/
			// driver.get("http://www.acc.org/" );
			// ScenarioForAcc.page_Wait();
			try {
				dRDriver.findElement(
						By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"))
						.click(); // click log in to my application
				ScenarioForAcc.page_Wait();
				Thread.sleep(5000);
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}

				String login = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				Thread.sleep(5000);
				if (login.contains("Login")) {
					logger.log("pass", "Login page displayed for acc", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// ScenarioForAcc.page_Wait();
			System.out.println("login screen captured");

			try {
				// dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
				// driver.findElement(By.id("UserName")).clear();
				// ScenarioForAcc.page_Wait();
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForNonMembership");
				Thread.sleep(5000);
				dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
						UserNameForMembership); // enters username data in
												// username field
				
				String username = dRDriver.findElement(
						By.xpath("//*[@id='UserName']")).getAttribute("value");
			
				System.out.println(username);
				//ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName" + '"' + username + '"'
							+ "successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// ScenarioForAcc.page_Wait();

			try {

				// dRDriver.findElement(By.id("Password")).click();
				// ScenarioForAcc.page_Wait();
				String PasswordForMembership = Utils
						.loadProperty("PasswordForNonMembership");
				Thread.sleep(5000);
				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.id("Password")).sendKeys(
						PasswordForMembership);// enters the password info
				
				System.out.println(PasswordForMembership);
				String password = dRDriver.findElement(By.id("Password"))
						.getAttribute("value");
			
				if (!password.isEmpty()) {
					logger.log("pass", "Password" + '"' + password + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// ScenarioForAcc.page_Wait();

			// page_Wait();

			try {

				dRDriver.findElement(
						By.xpath(".//*[@id='formLogin']/div[3]/p[3]/label")).click();// remember
			
				
				// me
																				// check

				// ScenarioForAcc.page_Wait();
				dRDriver.findElement(By.xpath(".//*[@id='formLogin']/div[3]/button"))
						.click(); // Click on login button
				Thread.sleep(15000);
				WebDriverWait wait=new WebDriverWait(dRDriver, 10);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='nav-myacc']/li[1]/span")));
				// et=logger.createTest("Login", "Login successful");
				// dRDriver.manage().timeouts().implicitlyWait(10,
				// TimeUnit.SECONDS);
			//	ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}
				System.out.println("Successfully logged in to ACC site");

				CapturingPopUp(dRDriver, browserName);
				// ScenarioForAcc.page_Wait();

				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				Thread.sleep(5000);
				
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			// ScenarioForAcc.page_Wait();

			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				Thread.sleep(5000);
				if (logoutLink.contains("Log Out")) {
					logger.log("pass", "The" + '"' + logoutLink + '"'
							+ " Link is displayed", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			// ScenarioForAcc.page_Wait();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void VerifyingEchoSap7WithoutLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et)

	{
		try {
			/*
			 * et = logger.createTest("VerifyECHO SAP 7 Navigation",
			 * "VerifyECHO SAP 7 Navigation Test");
			 */

			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "t");
			 */
			/*
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (dRDriver.getWindowHandles());
			 * dRDriver.switchTo().window(tabs2.get(1));
			 */

			ScenarioForAcc.LoggerForHomepageDisplay(logger, dRDriver, et);
			// newTabOpening(dRDriver);

			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}

			System.out.println("Validating Product: ECHOSap7 ");
			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx");
			CapturingPopUp(dRDriver,browserName);
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
		//	ScenarioForAcc.page_Wait();

			// try {
			try {

				
				String EchoSap7 = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_pnlHeader']/div/div[1]"))
						.getText();
				/*String PurchaseNow = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']"))
						.getText();*/
				//ScenarioForAcc.page_Wait();
				/*
				 * String LoginTab = dRDriver .findElement( By.xpath(
				 * ".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkLogin']"
				 * )) .getText();
				 */
				// ScenarioForAcc.page_Wait();
				if (EchoSap7.contains("EchoSAP 7")) {
					logger.log("pass", " Result for EchoSap7  displayed and button exists Without Login",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Results fo Echo Sap7 is not displayed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Results fo Echo Sap7 is not displayed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
			/*
			 * } catch (Exception e) {
			 * 
			 * e.printStackTrace(); }
			 */
			/*
			 * try { try { String MyACCTab = dRDriver .findElement( By.xpath(
			 * ".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_rptMyAcc_lnkName_0']"
			 * )) .getText(); ScenarioForAcc.page_Wait(); if
			 * (EchoSap7.contains("EchoSAP 7") && MyACCTab.contains("My ACC")) {
			 * logger.log("pass", " Result for EchoSap7  displayed and" +
			 * PurchaseNow + " button exists with Login", false, dRDriver,
			 * reportingPath, et); } } catch (Exception e1) { // TODO
			 * Auto-generated catch block // e1.printStackTrace();
			 * logger.log("fail", "Results fo Echo Sap7 is not displayed" + "\""
			 * + "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * // LogoutFeature(reportPath, dRDriver, logger, reportingPath,
			 * et);
			 * 
			 * }
			 * 
			 * 
			 * logger.flush(); } catch(Exception e) { e.printStackTrace(); }
			 */

		} catch (Exception e) {

			e.printStackTrace();
		}
		// newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);

	}
	public static void CapturingPopUp(WebDriver dRDriver, String browserName) {
	try {
		List<WebElement> loginPagePopUP = dRDriver.findElements(By
				.xpath(".//*[@id='acsMainInvite']/div/a[1]"));
		//ScenarioForAcc.page_Wait();
		
		if (browserName.equalsIgnoreCase("firefox")
							|| browserName.equalsIgnoreCase("chrome")) 
		{

		ScenarioForAcc.page_Wait();
		}
		else if(browserName.equalsIgnoreCase("ie"))
		{
			dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
		if (loginPagePopUP.size() > 0) {
			loginPagePopUP.get(0).click();
		
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			Thread.sleep(5000);
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	public static void VerifyingEchoSap7WithLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et)

	{
		try {

			try {
				dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx");
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				CapturingPopUp(dRDriver,browserName);
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				logger.log("pass", "Navigated to education echosap7 site",
						false, dRDriver, reportingPath, et);
			} catch (Exception e) {
				logger.log("fail",
						"Did not navigate to echosap7 education site", true,
						dRDriver, reportingPath, et);
			}

			logger.flush();
			try {

				
				/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}*/
				//ScenarioForAcc.page_Wait();
				String EchoSap7 = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_pnlHeader']/div/div[1]"))
						.getText();
				/*String PurchaseNow = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']"))
						.getAttribute("value");*/
				// ScenarioForAcc.page_Wait();
				String MyACCTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
						.getText();
				//ScenarioForAcc.page_Wait();
				if (EchoSap7.contains("EchoSAP 7")&& MyACCTab.contains("My ACC")) {
					logger.log("pass", " Result for EchoSap7  displayed & button exists with already Login", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Results fo Echo Sap7 is not displayed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {

				logger.log("fail", "Results fo Echo Sap7 is not displayed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
				//

			}

			logger.flush();
			// ScenarioForAcc.page_Wait();
			/*
			 * } catch(Exception e) { e.printStackTrace(); }
			 */

		} catch (Exception e) {

			e.printStackTrace();
		}
		System.out.println("going to close the opened tab");
		// newTabClosing(dRDriver);
		System.out.println("After closing tab ab");

	}

	public static void VerifyCVQualityWithoutLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			/*
			 * et = logger.createTest("VerifyCVQualityLoginPages",
			 * "CVQUALITY Test");
			 */
			// newTabOpening(dRDriver);

			dRDriver.get("http://cvquality.acc.org");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();

			try {
				/*String NCDRTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_0']"))
						.getText();

				String InitiativesTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_1']"))
						.getText();

				String CommunicationsKitTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_2']"))
						.getText();
				String ClinicalToolTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_3']"))
						.getText();*/

				String LoginTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='TopNavigation_TF9FBB7A2001_ctl00_ctl00_navigationUl']/li[5]/a"))
						.getText();
				Thread.sleep(5000);
				/*Thread.sleep(5000);
				Thread.sleep(5000);*/
				if (/*NCDRTab.contains("NCDR")
						&& InitiativesTab.contains("INITIATIVES")
						&& CommunicationsKitTab
								.contains("QI COMMUNICATIONS KIT")
						&& ClinicalToolTab.contains("CLINICAL TOOLKITS")
						&& */LoginTab.contains("LOGIN")) {
					logger.log("pass",
							"Successfully redirected to CV quality page and Login button exists", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "CVQuality page not displayed " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "CVQuality page not displayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			//ScenarioForAcc.page_Wait();

			/*
			 * try{
			 * 
			 * 
			 * String LogoutTab = dRDriver .findElement(
			 * By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
			 * .getText();
			 * 
			 * if (NCDRTab.contains("NCDR") &&
			 * InitiativesTab.contains("INITIATIVES") && CommunicationsKitTab
			 * .contains("QI COMMUNICATIONS KIT") &&
			 * ClinicalToolTab.contains("CLINICAL TOOLKITS") &&
			 * LogoutTab.contains("LOGOUT")) { logger.log("pass",
			 * "Successfully redirected to CV quality page and following tabs "
			 * +'"' + NCDRTab +'"' + " , " +'"' + InitiativesTab +'"' + " , "
			 * +'"' + CommunicationsKitTab +'"' + " , " +'"' + ClinicalToolTab
			 * +'"' + " exists after Logging in", false, dRDriver,
			 * reportingPath, et); } } catch(Exception e){ logger.log("fail",
			 * "CVQuality page not displayed " + "\"" + "\" doesn't exists.",
			 * true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();
			 */

		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);
	}

	public static void VerifyCVQualityWithLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			/*
			 * et = logger.createTest("VerifyCVQualityLoginPages",
			 * "CVQUALITY Test");
			 */
			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			// newTabOpening(dRDriver);
			try {
				dRDriver.get("http://cvquality.acc.org");
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				//ScenarioForAcc.page_Wait();
				
				logger.log("pass", "Navigated to cvquality page", false,
						dRDriver, reportingPath, et);
				
			} catch (Exception e) {
				logger.log("fail", "didnt navigate to cvquality page", true,
						dRDriver, reportingPath, et);
			}

			/*
			 * try {
			 * 
			 * 
			 * String LoginTab = dRDriver .findElement(
			 * By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnkLogin']"))
			 * .getText();
			 * 
			 * if (NCDRTab.contains("NCDR") &&
			 * InitiativesTab.contains("INITIATIVES") && CommunicationsKitTab
			 * .contains("QI COMMUNICATIONS KIT") &&
			 * ClinicalToolTab.contains("CLINICAL TOOLKITS") &&
			 * LoginTab.contains("LOGIN")) { logger.log("pass",
			 * "Successfully redirected to CV quality page and following tabs "
			 * +'"'+ NCDRTab+'"' + " , " +'"' + InitiativesTab +'"' + " , " +'"'
			 * + CommunicationsKitTab +'"' + " , " +'"' + ClinicalToolTab +'"' +
			 * " exists without Logging in", false, dRDriver, reportingPath,
			 * et); }} catch(Exception e) { logger.log("fail",
			 * "CVQuality page not displayed " + "\"" + "\" doesn't exists.",
			 * true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush(); ScenarioForAcc.page_Wait();
			 */
			// ScenarioForAcc.page_Wait();
			try {

				/*
				 * String LogoutTab = dRDriver .findElement(
				 * By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
				 * .getText();
				 */

				/*String NCDRTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_0']"))
						.getText();

				String InitiativesTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_1']"))
						.getText();

				String CommunicationsKitTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_2']"))
						.getText();
				String ClinicalToolTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_3']"))
						.getText();*/

				WebElement LogoutTab = dRDriver.findElement(By
						.xpath(".//*[@id='TopNavigation_TF9FBB7A2001_ctl00_ctl00_navigationUl']/li[5]/a"));

				Thread.sleep(5000);
//				Thread.sleep(5000);
				if (/*NCDRTab.contains("NCDR")
						&& InitiativesTab.contains("INITIATIVES")
						&& CommunicationsKitTab
								.contains("QI COMMUNICATIONS KIT")
						&& ClinicalToolTab.contains("CLINICAL TOOLKITS")
						&& */LogoutTab.isDisplayed()) {
					logger.log("pass",
							"Successfully redirected to CV quality page and logout link exists ", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "CVQuality page not displayed " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "CVQuality page not displayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			// ScenarioForAcc.page_Wait();

		} catch (Exception e) {

			e.printStackTrace();
		}
		// newTabClosing(dRDriver);

	}

	public static void logoutForCV(WebDriver dRDriver) {
		try {
			dRDriver.findElement(
					By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
					.click();
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void logoutForEchoSAP7(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkBtnLogout']"))
						.click();
//				ScenarioForAcc.page_Wait();
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{
				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				WebElement action1 = dRDriver.findElement(By
						.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
				if (action1.isDisplayed()) {
					System.out.println("Element displayed");

					logger.log("pass",
							"login button exists and clicked on login button",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login button is not clicked" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {

				// e.printStackTrace();
				logger.log("fail", "Login button is not clicked" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//ScenarioForAcc.page_Wait();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void VerifyACCScientificSessionWithoutLogin(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {

		try {
			/*
			 * et = logger.createTest("VerifyAnnualSpecificNavigation",
			 * "AnnualSpecific Test");
			 */

			// newTabOpening(dRDriver);

			dRDriver.get("http://accscientificsession.acc.org/acc.aspx");
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{
			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}

			// try {

			try {
				String RegisterNowLink = dRDriver.findElement(
						By.xpath(".//*[@id='header_nav']/ul/li[7]/a"))
						.getText();
				Thread.sleep(5000);
				String LoginLink = dRDriver.findElement(
						By.xpath(".//*[@id='login_btn']")).getText();

				Thread.sleep(5000);
				Thread.sleep(5000);
				if (LoginLink.contains("Login")
						&& RegisterNowLink.contains("Register Now")) {
					logger.log("pass",
							"Successfully navigated to annual specific page and "
									+ '"' + LoginLink + '"' + " link " + '"'
									+ RegisterNowLink + '"'
									+ " link exists without Logging in", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Annual specific page is not displayed "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail", "Annual specific page is not displayed "
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();
			//ScenarioForAcc.page_Wait();
			/*
			 * } catch (Exception e) {
			 * 
			 * e.printStackTrace(); }
			 */

			/*
			 * try {
			 * 
			 * 
			 * try{ String LogoutLink = dRDriver.findElement(
			 * By.xpath(".//*[@id='logout_btn']")).getText();
			 * 
			 * if (LogoutLink.contains("Logout") &&
			 * RegisterNowLink.contains("Register Now")) { logger.log("pass",
			 * "Successfully navigated to annual specific page and " +
			 * LogoutLink + " link " + RegisterNowLink +
			 * " link exists without Logging in", false, dRDriver,
			 * reportingPath, et); } }catch(Exception e) { logger.log("fail",
			 * "Annual specific page is not displayed " + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush(); }
			 * 
			 * catch(Exception e) { e.printStackTrace(); }
			 */

		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);
	}

	public static void VerifyACCScientificSessionWithLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			/*
			 * et = logger.createTest("VerifyAnnualSpecificNavigation",
			 * "AnnualSpecific Test");
			 */

			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			// ScenarioForAcc.page_Wait();
			// newTabOpening(dRDriver);

			dRDriver.get("http://accscientificsession.acc.org/acc.aspx");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{
			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();

			// dRDriver.get("http://accscientificsession.acc.org/acc.aspx");

			// ScenarioForAcc.page_Wait();

			/*
			 * try {
			 * 
			 * String LoginLink = dRDriver.findElement(
			 * By.xpath(".//*[@id='login_btn']")).getText(); try {
			 * 
			 * if (LoginLink.contains("Login") &&
			 * RegisterNowLink.contains("Register Now")) { logger.log("pass",
			 * "Successfully navigated to annual specific page and " +'"' +
			 * LoginLink +'"' + " link " +'"' + RegisterNowLink +'"' +
			 * " link exists without Logging in", false, dRDriver,
			 * reportingPath, et); } }catch(Exception e) { logger.log("fail",
			 * "Annual specific page is not displayed " + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush(); } catch (Exception e) {
			 * 
			 * e.printStackTrace(); }
			 */

			/*
			 * try {
			 */

			try {

				String RegisterNowLink = dRDriver.findElement(
						By.xpath(".//*[@id='header_nav']/ul/li[7]/a"))
						.getText();
				Thread.sleep(5000);
				String LogoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='logout_btn']")).getText();
				Thread.sleep(5000);
				
				if (LogoutLink.contains("Logout")
						&& RegisterNowLink.contains("Order iScience")) {
					logger.log("pass",
							"Successfully navigated to annual specific page and "
									+ '"' + LogoutLink + '"' + " link " + '"'
									+ RegisterNowLink + '"'
									+ " link exists without Logging in", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Annual specific page is not displayed "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
			} catch (Exception e) {
				logger.log("fail", "Annual specific page is not displayed "
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();

			/*
			 * }catch(Exception e) { e.printStackTrace(); }
			 */
			// ScenarioForAcc.page_Wait();

		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);

	}

	public static void VerifyJACCJournalWithoutLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {

			newTabOpening(dRDriver);
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			CapturingPopup(dRDriver);

			/*
			 * ScenarioForAcc.page_Wait();
			 * dRDriver.get("http://onlinejacc.org/journals.aspx");
			 */
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
			
		//	ScenarioForAcc.page_Wait();
			System.out.println("Validating JACC Journal link");

			try {
				String Search = dRDriver
						.findElement(
								By.xpath(".//*[@id='divUmbrellasiteHeader']/div[2]/div[1]/div[2]/div[1]/label"))
						.getText();

				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignIn']"))
						.getText();

				if (Login.contains("LOGIN") && Search.contains("SEARCH")) {
					logger.log(
							"pass",
							"JACC Journal page displayed without Login session",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "JACC Journal page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "JACC Journal page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			/*
			 * } catch (Exception e) { String Logout = dRDriver.findElement(
			 * By.xpath(".//*[@id='globalHeader_lbSignOut']")) .getText();
			 * 
			 * if (Logout.contains("SIGN OUT") && Search.contains("SEARCH")) {
			 * logger.log("pass", "JACC Journal page displayed with Login",
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "JACC Journal page is not displayed" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush(); e.printStackTrace(); }
			 */
			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyJACCJournalWithLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			newTabOpening(dRDriver);
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			CapturingPopup(dRDriver);

			/*
			 * ScenarioForAcc.page_Wait();
			 * dRDriver.get("http://onlinejacc.org/journals.aspx");
			 */
			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			System.out.println("Validating JACC Journal link");

			/*
			 * try { String Login = dRDriver.findElement(
			 * By.xpath(".//*[@id='globalHeader_lbSignIn']")) .getText();
			 * 
			 * if (Login.contains("LOGIN") && Search.contains("SEARCH")) {
			 * logger.log("pass", "JACC Journal page displayed without Login",
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "JACC Journal page is not displayed" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush(); }
			 */
			try {

				String Search = dRDriver
						.findElement(
								By.xpath(".//*[@id='divUmbrellasiteHeader']/div[2]/div[1]/div[2]/div[1]/label"))
						.getText();

				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']"))
						.getText();

				if (Logout.contains("SIGN OUT") && Search.contains("SEARCH")) {
					logger.log("pass",
							"JACC Journal page displayed with Login", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "JACC Journal page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "JACC Journal page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			/*
			 * } catch(Exception e)catch (Exception e) { logger.log("fail",
			 * "Navigation to annual diclosure page failed" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et); {
			 * e.printStackTrace(); }
			 */
			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{
			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		newTabClosing(dRDriver);

		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyACCSAP9WithoutLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			/*
			 * et = logger.createTest("VerifyACCSAP9Navigation",
			 * "VerifyACCSAP9Navigation Test");
			 */

			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			// newTabOpening(dRDriver);
			// dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			/*
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (dRDriver.getWindowHandles());
			 * dRDriver.switchTo().window(tabs2.get(1));
			 */
			//ScenarioForAcc.page_Wait();

			dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");

			System.out.println("Validating ACCSAP 9 link");

			try {

				String Header = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				Thread.sleep(5000);
				Thread.sleep(5000);
				//ScenarioForAcc.page_Wait();
				/*
				 * dRDriver.findElement(
				 * By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")).click();
				 * ScenarioForAcc.page_Wait();
				 */

				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();
				Thread.sleep(5000);
				Thread.sleep(5000);
				if (Header.contains("ACCSAP 9")
						&& Login.contains("Log in to MyACC")) {
					logger.log("pass", "ACCSAP 9 page displayed without Login",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			} catch (Exception e) {
				logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			/*
			 * } catch (Exception e) { String Logout = dRDriver.findElement(
			 * By.xpath(".//*[@id='header-personal']/p/a")).getText(); if
			 * (Header.contains("ACCSAP 9") && Logout.contains("Log Out")) {
			 * logger.log("pass", "ACCSAP 9 page displayed after Login", false,
			 * dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "ACCSAP 9 page is not displayed" + "\"" + "\" doesn't exists.",
			 * true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();}
			 */

			//ScenarioForAcc.page_Wait();
			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "w");
			 */

		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);

	}

	public static void VerifyACCSAP9WithLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			/*
			 * et = logger.createTest("VerifyACCSAP9Navigation",
			 * "VerifyACCSAP9Navigation Test");
			 */

			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			// newTabOpening(dRDriver);
			// dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			/*
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (dRDriver.getWindowHandles());
			 * dRDriver.switchTo().window(tabs2.get(1));
			 */
			// ScenarioForAcc.page_Wait();

			dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			System.out.println("Validating ACCSAP 9 link");

			/*
			 * try {
			 * 
			 * dRDriver.findElement(
			 * By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")).click();
			 * ScenarioForAcc.page_Wait();
			 * 
			 * String Login = dRDriver.findElement(
			 * By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")) .getText();
			 * if (Header.contains("ACCSAP 9") &&
			 * Login.contains("Log in to MyACC")) { logger.log("pass",
			 * "ACCSAP 9 page displayed without Login", false, dRDriver,
			 * reportingPath, et); } }catch(Exception e) { logger.log("fail",
			 * "ACCSAP 9 page is not displayed" + "\"" + "\" doesn't exists.",
			 * true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();}
			 */

			try {

				String Header = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

				// ScenarioForAcc.page_Wait();
				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				if (Header.contains("ACCSAP 9") && Logout.contains("Log Out")) {
					logger.log("pass", "ACCSAP 9 page displayed after Login",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			// ScenarioForAcc.page_Wait();
			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "w");
			 */

		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);

	}

	public static void RegistrationForMeetingFromEbiz(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {

			/*
			 * et = logger.createTest("RegistrationForaMeeting",
			 * "RegistrationForaMeeting Test");
			 */

			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "t");
			 * 
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (dRDriver.getWindowHandles());
			 * dRDriver.switchTo().window(tabs2.get(1));
			 */

			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			// newTabOpening(dRDriver);
			// ScenarioForAcc.page_Wait();
			dRDriver.get("https://www.acc.org/education-and-meetings/meetings/meeting-items/2016/03/17/09/04/2017-snowmass");
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}

			System.out.println("Validating Registration link");

			/*
			 * try { String Login = dRDriver.findElement(
			 * By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")) .getText();
			 * if (Header.contains("Cardiovascular Conference at Snowmass") &&
			 * Login.contains("Log in to MyACC")) { logger.log( "pass",
			 * "Registring for a meeting page displayed without Login", false,
			 * dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Registring for a meeting page is not displayed" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();}
			 */
			try {

				String Header = dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/article/h1"))
						.getText();

				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				if (Header.contains("Cardiovascular Conference at Snowmass")
						&& Logout.contains("Log Out")) {
					logger.log("pass", "Education-and-Meetings page displayed after Login",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Education-and-Meetings page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Education-and-Meetings page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			// }
			//ScenarioForAcc.page_Wait();

			try {
				dRDriver.findElement(
						By.xpath("//*[@id='accordion']/article[4]/div[1]/h1/a/span"))
						.click();
				/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();*/
				Thread.sleep(10000);
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				CapturingPopup(dRDriver);
				String Register = dRDriver.findElement(
						By.xpath(".//*[@id='box3']/p[3]")).getText();
				Thread.sleep(5000);
				if (Register.contains("click here")) {
					logger.log("pass",
							"Registration link is opened with Login", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Registration link is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Registration link is not opened " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "w");
			 */

			/*
			 * dRDriver.close(); dRDriver.switchTo().window(tabs2.get(0));
			 */

		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyACCSAP9OL(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {

			dRDriver.get("http://edu.acc.org/diweb/catalog/item/eid/ACCSAP9OL");
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}

			System.out.println("Validating ACCSAP 9 link");

			try {
				WebElement BuyNow = dRDriver.findElement(By
						.xpath("//input[@class='buy-button']"));

				Thread.sleep(5000);
				if (BuyNow.isDisplayed()) {
					logger.log("pass",
							"ACCSAP 9 link page displayed without Login",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "ACCSAP 9 link page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				logger.log("fail", "ACCSAP 9 link page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			// ScenarioForAcc.page_Wait();
			String winHandleBefore = dRDriver.getWindowHandle();

			try {
				dRDriver.findElement(By.xpath("//input[@class='buy-button']")).click();

				Thread.sleep(25000);
				
				//ScenarioForAcc.page_Wait();
				for(String winHandle : dRDriver.getWindowHandles()){
				    dRDriver.switchTo().window(winHandle);
				}
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				WebDriverWait wait = new WebDriverWait(dRDriver, 20);
				wait.until(ExpectedConditions.elementToBeClickable(By
						.xpath("//*[@id='main_0_ctl00_ButtonContinueShopping']")));
			    }
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}

				WebElement ContinueShopping = dRDriver
						.findElement(By
								.xpath(".//*[@id='main_0_ctl00_ButtonContinueShopping']"));

				Thread.sleep(5000);
				WebElement Logout = dRDriver.findElement(By
						.xpath(".//*[@id='myacc-holder']/div/div/p/a"));
				ScenarioForAcc.page_Wait1();
				if (Logout.isDisplayed() && ContinueShopping.isDisplayed()) {
					logger.log("pass", "Shopping page is opened with Login",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Shopping page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				
				logger.log("fail", "Shopping page is not opened " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			dRDriver.close();
			dRDriver.switchTo().window(winHandleBefore);

		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyECCOA2(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {

			/*
			 * et = logger.createTest("Verifying ECCOA2 Link",
			 * "Verifying ECCOA2 Link Test");
			 */

			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "t");
			 * 
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (dRDriver.getWindowHandles());
			 * dRDriver.switchTo().window(tabs2.get(1));
			 */
			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			// newTabOpening(dRDriver);

			// ScenarioForAcc.page_Wait();
			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2014/ECCOA2.aspx");
			//
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}

			System.out.println("Validating ECCOA2 link");

			try {

				String StartProgram = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_StartLink']"))
						.getText();

				if (StartProgram.contains("Start Program")) {
					logger.log("pass", "ECCOA2 link page displayed and " + '"'
							+ StartProgram + '"' + "is displayed", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "ECCOA2 link page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				logger.log("fail", "ECCOA2 link page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			// ScenarioForAcc.page_Wait();

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_StartLink']"))
						.click();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				//ScenarioForAcc.page_Wait();
				/*
				 * dRDriver.switchTo().frame(
				 * dRDriver.findElement(By.xpath(".//*[@id='form1']")));
				 */
				dRDriver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);
				// dRDriver.switchTo().frame("_fsControlFrame");

				// dRDriver.switchTo().frame(dRDriver.findElement(By.id("form1")));

				// ScenarioForAcc.page_Wait();

				if (dRDriver
						.findElements(By.xpath("//a[@id='fancybox-close']"))
						.size() > 0
						&& dRDriver
								.findElements(
										By.xpath("//a[@id='fancybox-close']"))
								.get(0).isDisplayed()) {
					dRDriver.findElements(By.xpath("//a[@id='fancybox-close']"))
							.get(0).click();
				
					if (browserName.equalsIgnoreCase("firefox")
										|| browserName.equalsIgnoreCase("chrome")) 
					{

					ScenarioForAcc.page_Wait();
					}
					else if(browserName.equalsIgnoreCase("ie"))
					{
						dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					}
					//ScenarioForAcc.page_Wait();
				}

				WebElement ActivcationPage = dRDriver.findElement(By
						.xpath(".//*[@id='header']"));

				/*
				 * WebElement ReqInformation = dRDriver .findElement(By
				 * .xpath(".//*[@id='content_0_divEulaInstruction']/span/strong"
				 * ));
				 */
				if (ActivcationPage.isDisplayed()) {
					logger.log("pass",
							"Activation page is opened without Login", false,
							dRDriver, reportingPath, et);
				}
				else
					
				{
					logger.log("fail", "Activation page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Activation page is not opened " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
				// e.printStackTrace();
			}
			logger.flush();

			/*
			 * try{ String LoginHeader = dRDriver.findElement(
			 * By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
			 * 
			 * String Login = dRDriver.findElement(
			 * By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")) .getText();
			 * if (Login.contains("Login") &&
			 * LoginHeader.contains("Log in to MyACC")) { logger.log("pass",
			 * "Login page is opened ", false, dRDriver, reportingPath, et); } }
			 * catch (Exception e1) { // TODO Auto-generated catch block //
			 * e1.printStackTrace(); logger.log("fail",
			 * "Login page is not opened " + "\"" + "\" doesn't exists.", true,
			 * dRDriver, reportingPath, et);
			 * 
			 * }
			 * 
			 * 
			 * logger.flush();
			 */

			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "w");
			 *//*
				 * dRDriver.findElement(By.xpath("//body")).sendKeys(
				 * Keys.CONTROL + "w");
				 */

			/*
			 * dRDriver.close(); dRDriver.switchTo().window(tabs2.get(0));
			 */
		} catch (Exception e) {

			e.printStackTrace();
		}
		// newTabClosing(dRDriver);

		redirectToHomePage(logger, et, dRDriver);

	}

	public static void VerifyingPurchaseLink(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			/*
			 * VerifyingEchoSap7(reportPath, dRDriver, logger, reportingPath,
			 * et); ScenarioForAcc.page_Wait();
			 */
			/*
			 * et =
			 * logger.createTest("VerifyECHO SAP 7 Navigation Without Login",
			 * "VerifyECHO SAP 7 Navigation Without Login Test");
			 */

			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "t");
			 * 
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (dRDriver.getWindowHandles());
			 * dRDriver.switchTo().window(tabs2.get(1));
			 */
			// ScenarioForAcc.page_Wait();
			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			// newTabOpening(dRDriver);
			// ScenarioForAcc.page_Wait();

			//dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx");
			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2012/CathSAP-4.aspx?w_nav=LC");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			CapturingPopUp(dRDriver, browserName);
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			System.out.println("Validating Product: ECHOSap7 ");

			/*
			 * try { String LoginTab = dRDriver .findElement( By.xpath(
			 * ".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkLogin']"))
			 * .getText(); try {
			 * 
			 * ScenarioForAcc.page_Wait(); if (EchoSap7.contains("EchoSAP 7") &&
			 * LoginTab.contains("Log in to MyACC")) { logger.log("pass",
			 * " Result for EchoSap7  displayed and" + PurchaseNow +
			 * " button exists", false, dRDriver, reportingPath, et); } } catch
			 * (Exception e) { // TODO Auto-generated catch block //
			 * e.printStackTrace(); logger.log("fail",
			 * "Results fo Echo Sap7 is not displayed" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush(); } try {
			 */

			try {
				String EchoSap7 = dRDriver
						.findElement(
								By.xpath(".//*[@id='lvl2-masthead']/h1"))
						.getText();
				/*String PurchaseNow = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']"))
						.getText();*/

				String MyACCTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
						.getText();

				//ScenarioForAcc.page_Wait();
				if (EchoSap7.contains("CathSAP") && MyACCTab.contains("My ACC")) {
					logger.log("pass", " Result for cathsap4  displayed and button exists", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Results fo Echo Sap7 is not displayed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				logger.log("fail", "Results fo Echo Sap7 is not displayed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			// ScenarioForAcc.page_Wait();
			// }

			// try{
			try {
				String winHandleBefore = dRDriver.getWindowHandle();
				Thread.sleep(5000);
				dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/table[2]/tbody/tr/td/table/tbody/tr[2]/td/div/a"))
						.click();
				Thread.sleep(25000);
				/*WebDriverWait wait=new WebDriverWait(dRDriver, 10);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='id2']")));*/
			
				for(String winHandle : dRDriver.getWindowHandles()){
				    dRDriver.switchTo().window(winHandle);
				}
				element=dRDriver.findElement(By.xpath(".//*[@id='id2']"));
				boolean ele=waitForURLToMatch(element, 10);
				while(ele!=true)
				{
					Thread.sleep(3000);
				}
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				ScenarioForAcc.page_Wait();
				try {
					WebElement shoppingLink=dRDriver.findElement(By.xpath(".//*[@id='id2']"));
					Thread.sleep(5000);
					
					if (shoppingLink.isDisplayed()) {
						logger.log("pass",
								"Shopping cart page is opened without Login",
								false, dRDriver, reportingPath, et);
					}
					else
					{
						logger.log("fail", "Shopping cart page is not opened " + "\""
								+ "\" doesn't exists.", true, dRDriver, reportingPath,
								et);
					}
				} catch (Exception e) {
					logger.log("fail", "Shopping cart page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
					e.printStackTrace();
					System.out.println("exception occured");
				}
				dRDriver.close();

				Thread.sleep(5000);
				dRDriver.switchTo().window(winHandleBefore);
			/*	WebElement shoppingLink;
				WebElement GotoCart;
				try {
					shoppingLink = dRDriver
							.findElement(By
									.xpath(".//*[@id='main_0_ctl00_ButtonContinueShopping']	"));
					GotoCart = dRDriver
							.findElement(By
									.xpath(".//*[@id='main_0_ctl00_SegmentTabList_i0']/span"));
					if (shoppingLink.isDisplayed() && GotoCart.isDisplayed()) {
						logger.log("pass",
								"Shopping cart page is opened without Login",
								false, dRDriver, reportingPath, et);
					}
					else
					{
						logger.log("fail", "Ebiz shopping page is not opened " + "\""
								+ "\" doesn't exists.", true, dRDriver, reportingPath,
								et);
					}
				} catch (Exception e) {
					logger.log("fail", "Ebiz shopping page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}

				*/
			} catch (Exception e) {
				logger.log("fail", "shopping cart page is not opened " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

				e.printStackTrace();
			}
			logger.flush();
			/*
			 * } catch(Exception e) { try{ String LoginHeader =
			 * dRDriver.findElement(
			 * By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
			 * ScenarioForAcc.page_Wait(); String Login = dRDriver.findElement(
			 * By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")) .getText();
			 * if (Login.contains("Login") &&
			 * LoginHeader.contains("Log in to MyACC")) { logger.log("pass",
			 * "Login page is opened ", false, dRDriver, reportingPath, et); }}
			 * catch(Exception e1){ logger.log("fail",
			 * "Login page is not opened " + "\"" + "\" doesn't exists.", true,
			 * dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();
			 * 
			 * 
			 * 
			 * 
			 * }
			 */

			/*
			 * dRDriver.close(); dRDriver.switchTo().window(tabs2.get(0));
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// newTabClosing(dRDriver);

		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyingEbizforMeetingAndProductId(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			/*
			 * et = logger.createTest("Verifying Ebiz for meeting Link",
			 * "Verifying Ebiz meeting and product Id Link Test");
			 */

			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "t");
			 * 
			 * ArrayList<String> tabs2 = new ArrayList<String>
			 * (dRDriver.getWindowHandles());
			 * dRDriver.switchTo().window(tabs2.get(1));
			 */

			// LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			// newTabOpening(dRDriver);

			// ScenarioForAcc.page_Wait();
			dRDriver.get("https://ebiz.acc.org/Meetings?ProductId=41118240");
			ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}

			//System.out.println("Validating Ebiz meeting and product Id link");

			try {
				/*WebElement Header = dRDriver
						.findElement(By
								.xpath(".//*[@id='main_0_ctl00_cMtgRegWizContactInfo_MtgRegWizHeader_lblMtgName']"));*/

				String Logout = dRDriver
						.findElement(
								By.xpath(".//*[@id='main_0_ctl00_cMtgRegWizContactInfo_MtgRegWizHeader_lblMtgName']"))
						.getText();

				Thread.sleep(5000);
				if (!Logout.isEmpty()) {
					logger.log("pass",
							"Ebiz meeting and product Id link page displayed with Login session and"
									+ '"' + Logout + '"' + "link exists ",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Ebiz meeting and product Id link is not displayed"
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Ebiz meeting and product Id link is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			
			// ScenarioForAcc.page_Wait();
		} catch (Exception e) {

			e.printStackTrace();
		}

		// newTabClosing(dRDriver);

		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyingBasicLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {
			et = logger.createTest("VerifyingBasicLogin",
					"VerifyingBasicLogin Test");
			System.out.println("Scenario1");

			System.out.println("drDriver is " + dRDriver);

			BasicLoginFeature(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait1();
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

			ScenarioForAcc.page_Wait1();
			verifyLoginForCVQuality(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();

			VerifyCVQualityWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();

			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait1();

			VerifyCVQualityWithoutLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();

			verifyLoginForACCScientific(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();

			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();

			LogoutFeatureForACCScientific(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			verifyLoginForACCSAP9(reportPath, dRDriver, logger, reportingPath,
					et);
			ScenarioForAcc.page_Wait1();
			VerifyACCSAP9WithLogin(reportPath, dRDriver, logger, reportingPath,
					et);
			ScenarioForAcc.page_Wait1();
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait1();
			BasicLoginFeature(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait1();
			verifyingDisclosureLink(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait1();
			BasicLoginFeatureForEchoSAP7(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
			
			ScenarioForAcc.page_Wait1();
			/*
			*/
			/*
			 * VerifyingEchoSap7WithoutLogin(reportPath, dRDriver, logger,
			 * reportingPath, et);
			 
			


			
			 * verifyLoginForJACCJournal(reportPath, dRDriver, logger,
			 * reportingPath, et); VerifyJACCJournalWithLogin(reportPath,
			 * dRDriver, logger, reportingPath, et);
			 * LogoutFeatureForJACCJournal(reportPath, dRDriver, logger,
			 * reportingPath, et);
			 

			

			
*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void VerifyingBasicSSOConnection(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {
			System.out.println("Scenario2");

			et = logger.createTest("VerifyingBasicSSOConnection",
					"VerifyingBasicSSOConnection Test");

			BasicLoginFeature(reportPath, dRDriver, logger, reportingPath, et);

			ScenarioForAcc.page_Wait1();
			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			verifyingDisclosureLink(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			VerifyCVQualityWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			VerifyACCSAP9WithLogin(reportPath, dRDriver, logger, reportingPath,
					et);
			ScenarioForAcc.page_Wait1();
			RegistrationForMeetingFromEbiz(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			/*
			
			
			
			
			 * VerifyJACCJournalWithLogin(reportPath, dRDriver, logger,
			 * reportingPath, et);
			 

			*/
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static WebDriver VerifyRememberMeFunctionlity(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {
			// ScenarioForAcc.page_Wait();
			RememberMeFeatureForNonMember(reportingPath, dRDriver, logger,
					reportingPath, et);
			/*
			 * et = logger.createTest("Verifying RememberMe Functionality",
			 * "Verifying RememberMe Functionality");
			 */
			/*
			 * dRDriver.findElement(By.xpath("//body")).sendKeys( Keys.CONTROL +
			 * "t");
			 */
			ScenarioForAcc.page_Wait1();
			dRDriver.get("http://author.education.acc.org/sitecore/login");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			CapturingPopUp(dRDriver, browserName);
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
		ScenarioForAcc.page_Wait();


			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
		//	ScenarioForAcc.page_Wait();
			/*
			 * String logoutLink = dRDriver.findElement(
			 * By.xpath(".//*[@id='header-personal']/p/a")).getText();
			 * 
			 * if (logoutLink.contains("Log Out")) { logger.log("pass",
			 * logoutLink + " Link is displayed and user still logged in",
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Login is not successfull" + "\"" + "\" doesn't exists.", true,
			 * dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();
			 */

			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				Thread.sleep(5000);
				
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log(
							"pass",
							"Login session exits after navigating to other website",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			ScenarioForAcc.page_Wait1();
			// Set<Cookie> cookies1= dRDriver.manage().getCookies();
			// dRDriver.manage().timeouts().implicitlyWait(30000,
			// TimeUnit.SECONDS);
			// ScenarioForAcc.page_Wait();

			Set<Cookie> allCookies1 = null;

			
			System.out.println("browserName:" + browserName);

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
				allCookies1 = dRDriver.manage().getCookies();
			}

			// Set<Cookie> allCookies = dRDriver.manage().getCookies();
			dRDriver.close();

			// dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			// dRDriver = OpenFFBrowser(dRDriver, capabilities);
			/*
			 * dRDriver= new FirefoxDriver(); for (Cookie cookie : cookies1) {
			 * dRDriver.manage().addCookie(cookie); }
			 */

			if (browserName.equalsIgnoreCase("firefox")) {
				dRDriver = OpenGeckoBrowser(dRDriver, capabilities);
			} else if (browserName.equalsIgnoreCase("chrome")) {
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else {
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}

			dRDriver.get("http://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
		CapturingPopUp(dRDriver, browserName);
			Thread.sleep(5000);
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
				for (Cookie cookie : allCookies1) {
					try {
						dRDriver.manage().addCookie(cookie);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			/*
			 * for(Cookie cookie : allCookies) {
			 * dRDriver.manage().addCookie(cookie); }
			 */
			// cookieManager();
			/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();*/
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait1();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
					dRDriver.navigate().refresh();
				}
			// dRDriver.manage().window().maximize();

			// String ChildWindow = dRDriver.getWindowHandle();

			// String
			// logoutLink=dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).getText();
			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				Thread.sleep(5000);
			 
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log(
							"pass",
							"Login session exits after nclosing and reopening the browser",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			// dRDriver.switchTo().window(ChildWindow);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static WebDriver VerifyRememberMeFunctionlityonACC(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception {

		et = logger.createTest("VerifyRememberMeFunctionlityonACC",
				"VerifyRememberMeFunctionlityonACC");
		dRDriver = VerifyRememberMeFunctionlity(reportPath, dRDriver, logger,
				reportingPath, et);

		VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath,
				et);
		ScenarioForAcc.page_Wait1();
		VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath,
				et);
		ScenarioForAcc.page_Wait1();
		// VerifyJACCJournalWithLogin(reportPath, dRDriver, logger,
		// reportingPath, et);
		VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
				reportingPath, et);
		ScenarioForAcc.page_Wait1();
		verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger,
				reportingPath, et);
		ScenarioForAcc.page_Wait1();
		LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
		return dRDriver;

	}

	public static WebDriver VerifyRememberMeFunctionalityIfUserVisitsLLP(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {

		try {
			System.out.println("Scenario3");

			et = logger.createTest(
					"VerifyRememberMeFunctionalityIfUserVisitsLLP",
					"VerifyRememberMeFunctionalityIfUserVisitsLLP Test");

			RememberMeFeature(reportingPath, dRDriver, logger, reportingPath,
					et);
			ScenarioForAcc.page_Wait1();
			VerifyingPurchaseLink(reportingPath, dRDriver, logger,
					reportingPath, et);

			// ScenarioForAcc.page_Wait();
			ScenarioForAcc.page_Wait1();
			dRDriver.get("http://author.education.acc.org/sitecore/login");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();

			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();

			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log(
							"pass",
							"Login session exits after navigating to other website",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			Set<Cookie> allCookies2 = null;
		

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
				allCookies2 = dRDriver.manage().getCookies();
			}

			// Set<Cookie> allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
			ScenarioForAcc.page_Wait1();
			// dRDriver = OpenChrmBrowser(dRDriver, capabilities);

			if (browserName.equalsIgnoreCase("firefox")) {
				dRDriver = OpenFirefoxBrowser(dRDriver);
			} else if (browserName.equalsIgnoreCase("chrome")) {
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else {
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}

			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			

			CapturingPopUp(dRDriver, browserName);
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
				for (Cookie cookie : allCookies2) {
					try {
						dRDriver.manage().addCookie(cookie);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			// cookieManager();
			/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();*/
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
					dRDriver.navigate().refresh();
				}

			ScenarioForAcc.page_Wait1();

			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log(
							"pass",
							"Login session exits after closing and reopening the browser",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			VerifyCVQualityWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			// VerifyJACCJournalWithLogin(reportPath, dRDriver, logger,
			// reportingPath, et);
			VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
			verifyingDisclosureLink(reportPath, dRDriver, logger,
					reportingPath, et);
			VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger,
					reportingPath, et);
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;
	}

	public static WebDriver VerifyRememberMeDoesNotBreakAccesstoLLPProducts(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception

	{

		et = logger.createTest(
				"VerifyRememberMeDoesNotBreakAccesstoLLPProducts",
				"VerifyRememberMeDoesNotBreakAccesstoLLPProducts");

		dRDriver = VerifyRememberMeFunctionlity(reportPath, dRDriver, logger,

				reportingPath, et);

		VerifyECCOA2(reportPath, dRDriver, logger, reportingPath, et);
		ScenarioForAcc.page_Wait1();
		VerifyingPurchaseLink(reportPath, dRDriver, logger, reportingPath, et);
		LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
		return dRDriver;
	}

	public static WebDriver VerifyRememberMeFunctionalityonFederatedLogin(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {
		try {

			/*
			 * et = logger.createTest("VerifyingFederatedLogin",
			 * "VerifyingFederatedLogin Test");
			 */
			dRDriver.get("http://auth.acc.org/ACCFederatedLogin/Login?SP=https://sp.silverchair.com/jacc/production/shibboleth&src=Silverchair&targeturl=/journals.aspx");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			//ScenarioForAcc.page_Wait();

			try {
				// dRDriver.findElement(By.xpath(".//*[@id='globalHeader_lbSignIn']")).click();
				/*dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
				ScenarioForAcc.page_Wait();*/
				// driver.findElement(By.id("UserName")).clear();

				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
						UserNameForMembership);

				String username = dRDriver.findElement(
						By.xpath("//*[@id='UserName']")).getAttribute("value");

				System.out.println(username);
				ScenarioForAcc.page_Wait1();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName" + '"' + username + '"'
							+ "successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			ScenarioForAcc.page_Wait1();
			try {
				/*dRDriver.findElement(By.id("Password")).click();
				ScenarioForAcc.page_Wait();*/
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.id("Password")).sendKeys(
						PasswordForMembership);

				String password = dRDriver.findElement(By.id("Password"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password" + '"' + password + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// page_Wait();

			// CapturingPopup(dRDriver);

			ScenarioForAcc.page_Wait1();
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='formLogin']/p[4]/label")).click();
			
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				//ScenarioForAcc.page_Wait();
				dRDriver.findElement(By.xpath(".//*[@id='formLogin']/input[3]"))
						.click();

				// et=logger.createTest("Login", "Login successful");
				//ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				System.out.println("Successfully logged in to ACC site");
				//ScenarioForAcc.page_Wait();
				AlertForFederatedLogin();
				WebElement logoforJacc = dRDriver.findElement(By
						.xpath(".//*[@id='siteLogo']"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforJacc.isDisplayed()) {
					logger.log("pass", "Successfully logged into JACC", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for JACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for JACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			//ScenarioForAcc.page_Wait();

			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']"))
						.getText();

				Thread.sleep(5000);
				if (logoutLink.contains("SIGN OUT")) {
					logger.log("pass", logoutLink + " Link is displayed",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			//ScenarioForAcc.page_Wait();
			dRDriver.get("http://author.education.acc.org/sitecore/login");
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']"))
						.getText();

				if (logoutLink.contains("SIGN OUT")) {
					logger.log("pass", "The " + '"' + logoutLink + '"'
							+ " Link is displayed", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Login is not successfull after navigating to other website"
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail",
						"Login is not successfull after navigating to other website"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
			dRDriver.close();
			dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']"))
						.getText();

				if (logoutLink.contains("SIGN OUT")) {
					logger.log(
							"pass",
							"The"
									+ '"'
									+ logoutLink
									+ '"'
									+ " Link is displayed after closing and reopening the browser",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
			dRDriver.close();
			dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			dRDriver.get("http://www.acc.org");
		//	ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static WebDriver scenariosForFederatedLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		et = logger.createTest("scenariosForFederatedLogin",
				"scenariosForFederatedLogin Test");

		dRDriver = VerifyRememberMeFunctionalityonFederatedLogin(reportingPath,
				dRDriver, logger, reportingPath, et);

		VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath,
				et);
		VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath,
				et);
		VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
				reportingPath, et);
		verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
		VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);

		VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger,
				reportingPath, et);
		LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
		return dRDriver;
	}

	public static WebDriver VerifyRememberMeFunctionalityonScientificSession(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {

		try {

			et = logger.createTest(
					"VerifyRememberMeFunctionalityonScientificSession",
					"Login Test");

			dRDriver.get("https://accscientificsession.acc.org/");
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			WebDriverWait wait = new WebDriverWait(dRDriver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(By
					.xpath("//*[@id='login_btn']")));
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			try {
				WebElement logout = dRDriver.findElement(By
						.xpath("//*[@id='login_btn']"));
				WebElement MyAcc = dRDriver.findElement(By
						.xpath("//*[@id='header_nav']/ul/li[7]/a"));

				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log(
							"pass",
							"Successfully Navigated to acc Scientific session page",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Not successfully navigated to accscientific page"
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail",
						"Not successfully navigated to accscientific page"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();
			// ScenarioForAcc.page_Wait();

			// dRDriver.findElement(By.xpath(".//*[@id='login_btn']")).click();
			try {
				// dRDriver.findElement(By.xpath(".//*[@id='username']")).click();
				// driver.findElement(By.id("UserName")).clear();
				// ScenarioForAcc.page_Wait();
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath(".//*[@id='username']"))
						.sendKeys(UserNameForMembership);

				String username = dRDriver.findElement(
						By.xpath(".//*[@id='username']")).getAttribute("value");
				dRDriver.manage().timeouts()
						.implicitlyWait(10000, TimeUnit.SECONDS);
				System.out.println(username);
				// ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName" + '"' + username + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// ScenarioForAcc.page_Wait();

			try {
				// dRDriver.findElement(By.id("password")).click();
				// ScenarioForAcc.page_Wait();
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.id("password")).sendKeys(
						PasswordForMembership);
				dRDriver.manage().timeouts()
						.implicitlyWait(10000, TimeUnit.SECONDS);
				String password = dRDriver.findElement(By.id("password"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password" + '"' + password + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			// ScenarioForAcc.page_Wait();

			// CapturingPopup(dRDriver);
			try {

				dRDriver.findElement(
						By.xpath("//*[@id='login_form']/div/div[4]/label"))
						.click();

				// ScenarioForAcc.page_Wait();
				dRDriver.findElement(By.xpath("//*[@id='login_btn']")).click();
				//ScenarioForAcc.page_Wait();
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{
				WebDriverWait wait3 = new WebDriverWait(dRDriver, 20);
				wait3.until(ExpectedConditions.elementToBeClickable(By
						.xpath(".//*[@id='logout_btn']")));
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				// et=logger.createTest("Login", "Login successful");

				System.out.println("Successfully logged in to ACC site");

				// ScenarioForAcc.page_Wait();
				WebElement logout = dRDriver.findElement(By
						.xpath(".//*[@id='logout_btn']"));
				WebElement MyAcc = dRDriver.findElement(By
						.xpath(".//*[@id='login_link']"));

				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			// ScenarioForAcc.page_Wait();
			// Navigating to other website
			try {

				dRDriver.get("http://author.education.acc.org/sitecore/login");
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}
				//ScenarioForAcc.page_Wait();
				logger.log(
						"pass",
						"Successfully navigated to other website - education author page",
						false, dRDriver, reportingPath, et);
			} catch (Exception e) {
				logger.log("fail", "Not Navigated to education author page",
						true, dRDriver, reportingPath, et);
			}
			logger.flush();
			// Going back to home website
			try {
				dRDriver.get("https://accscientificsession.acc.org/");
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{
				WebDriverWait wait1 = new WebDriverWait(dRDriver, 20);
				wait1.until(ExpectedConditions.elementToBeClickable(By
						.xpath("//*[@id='logout_btn']")));
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				}
				
				//ScenarioForAcc.page_Wait();
				WebElement logout = dRDriver.findElement(By
						.xpath(".//*[@id='logout_btn']"));
				WebElement MyAcc = dRDriver.findElement(By
						.xpath(".//*[@id='login_link']"));
				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log(
							"pass",
							"Successfully logged into ACC after navigating to other website",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			// dRDriver.get("https://accscientificsession.acc.org/");
			// ScenarioForAcc.page_Wait();
			// /######

		
			Set<Cookie> allCookies3 = null;

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
				allCookies3 = dRDriver.manage().getCookies();
			}

			// Set<Cookie> allCookies = dRDriver.manage().getCookies();
			try {
				dRDriver.close();
				logger.log("pass", "Successfully closed the browser", false,
						dRDriver, reportingPath, et);
			} catch (Exception e1) {
				logger.log("fail", "Browser not closed", true, dRDriver,
						reportingPath, et);
			}
			logger.flush();
			if (browserName.equalsIgnoreCase("firefox")) {
				dRDriver = OpenFirefoxBrowser(dRDriver);
			} else if (browserName.equalsIgnoreCase("chrome")) {
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else {
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}

			// dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			// dRDriver = OpenFFBrowser(dRDriver, capabilities);
			dRDriver.get("https://accscientificsession.acc.org/");
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) 
			{
				WebDriverWait wait2 = new WebDriverWait(dRDriver, 20);
				wait2.until(ExpectedConditions.elementToBeClickable(By
						.xpath("//*[@id='login_btn']")));
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			
			//ScenarioForAcc.page_Wait1();
//			dRDriver.manage().window().maximize();

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
				for (Cookie cookie : allCookies3) {
					try {
						try {
							dRDriver.manage().addCookie(cookie);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			ScenarioForAcc.page_Wait1();
			dRDriver.navigate().refresh();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			//ScenarioForAcc.page_Wait();
			/*
			 * if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome)) {
			 * for(Cookie cookie : allCookies) {
			 * dRDriver.manage().addCookie(cookie); } }
			 * 
			 * 
			 * //cookieManager(); Thread.sleep(500);
			 * dRDriver.navigate().refresh();
			 */

			try {
				WebElement logout = dRDriver.findElement(By
						.xpath(".//*[@id='logout_btn']"));
				WebElement MyAcc = dRDriver.findElement(By
						.xpath(".//*[@id='login_link']"));
				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log(
							"pass",
							"Successfully logged into ACC after closing and reopening the browser",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			/*dRDriver.close();
			if (browserName.equalsIgnoreCase("firefox")) {
				dRDriver = OpenFirefoxBrowser(dRDriver);
			} else if (browserName.equalsIgnoreCase("chrome")) {
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else {
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}*/
			dRDriver.get("http://www.acc.org");
			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
		/*	if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) {
				for (Cookie cookie : allCookies) {
					dRDriver.manage().addCookie(cookie);
				}
			}
			ScenarioForAcc.page_Wait1();
			dRDriver.navigate().refresh();*/

			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span")); // String
																		// //
			/*	header = dRDriver.findElement(
						By.xpath("//*[@id='myacc-holder']/div/div/p/a"))
						.getText();*/
				if (logoforacc.isDisplayed()) {
					logger.log(
							"pass",
							"Login session exits after closing and reopening the browser of other website",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			/*
			 * allCookies = null;
			 * 
			 * if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome)) {
			 * allCookies = dRDriver.manage().getCookies(); }
			 * 
			 * //allCookies = dRDriver.manage().getCookies(); dRDriver.close();
			 * // dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			 * 
			 * if(dRDriver.equals(driverFF)) {
			 * dRDriver=OpenFirefoxBrowser(dRDriver); } else
			 * if(dRDriver.equals(driverChrome)) { dRDriver =
			 * OpenChrmBrowser(dRDriver, capabilities); } else
			 * if(dRDriver.equals(driverInternet)){ dRDriver =
			 * OpenIEBrowser(dRDriver, capabilities); }
			 * 
			 * //dRDriver = OpenFFBrowser(dRDriver,capabilities);
			 * dRDriver.get("http://www.acc.org");
			 * dRDriver.manage().window().maximize();
			 * 
			 * if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome)) {
			 * for(Cookie cookie : allCookies) {
			 * dRDriver.manage().addCookie(cookie); } }
			 * 
			 * 
			 * //cookieManager(); Thread.sleep(500);
			 * dRDriver.navigate().refresh();
			 * 
			 * try { WebElement logoforacc = dRDriver.findElement(By
			 * .xpath(".//*[@id='nav-myacc']/li[1]/span")); // String //
			 * header=dRDriver
			 * .findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a"
			 * )).getText(); if (logoforacc.isDisplayed()) { logger.log("pass",
			 * "Login session exits after closing and reopening the browser of other website"
			 * , false, dRDriver, reportingPath, et); } } catch (Exception e) {
			 * // TODO Auto-generated catch block // e.printStackTrace();
			 * logger.log("fail", "Login failed for ACC" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * 
			 * } logger.flush();
			 */ScenarioForAcc.page_Wait1();
			 

			VerifyCVQualityWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			/*VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait();*/
			verifyingDisclosureLink(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait1();
			VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger,
					reportingPath, et);
			ScenarioForAcc.page_Wait1();
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static WebDriver VerifyRememberMeFunctionalityonQII(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {

		try {
			et = logger.createTest("VerifyRememberMeFunctionalityonQII",
					"Login Test");

			dRDriver.get("http://cvquality.acc.org/Login");

			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			try {

				WebElement LogIn = dRDriver.findElement(By
						.xpath(".//*[@id='TopNavigation_T254828A4001_ctl00_ctl00_navigationUl']/li[5]/a"));
				Thread.sleep(5000);
				Thread.sleep(5000);
				if (LogIn.isDisplayed()) {
					LogIn.click();
					logger.log("pass", "Homepage displayed for CVQuality",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Homepage not displayed for CVQuality"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail", "Homepage not displayed for CVQuality"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			// ScenarioForAcc.page_Wait();

			try {
				// dRDriver.findElement(By.xpath("//*[@id='msTopNav_rptPrimary_hypLnkLogin']")).click();
				// ScenarioForAcc.page_Wait();

				// dRDriver.findElement(By.xpath(".//*[@id='content_0_txtUsername']")).click();
				// driver.findElement(By.id("UserName")).clear();
				// ScenarioForAcc.page_Wait();
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtUsername']"))
						.sendKeys(UserNameForMembership);

				String username = dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtUsername']"))
						.getAttribute("value");

				System.out.println(username);
				// ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName" + '"' + username + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			// ScenarioForAcc.page_Wait();
			try {
				// dRDriver.findElement(By.xpath(".//*[@id='content_0_txtPassword']")).click();
				// ScenarioForAcc.page_Wait();
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtPassword']"))
						.sendKeys(PasswordForMembership);

				String password = dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtPassword']"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password" + '"' + password + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			// ScenarioForAcc.page_Wait();

			// page_Wait();

			// CapturingPopup(dRDriver);
			try {

				dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_chbxRememberMe']"))
						.click();

				Thread.sleep(5000);
				ScenarioForAcc.page_Wait();
				dRDriver.findElement(By.xpath(".//*[@id='ContentPlaceHolder1_C017_btnLogin']"))
						.click();

				Thread.sleep(15000);
				Thread.sleep(15000);
				Thread.sleep(5000);
				Thread.sleep(5000);
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

					WebDriverWait wait4 = new WebDriverWait(dRDriver, 20);
					wait4.until(ExpectedConditions.elementToBeClickable(By
							.xpath(".//*[@id='site-header']/div/div[1]/p/a")));
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				
				System.out.println("Successfully logged in to CV Quality site");
				String MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='site-header']/div/div[1]/p/a")).getText();
				Thread.sleep(5000);
				String LogOut = dRDriver.findElement(
						By.xpath(".//*[@id='qiiLoginStatus_lnkLogout']")).getText();
				Thread.sleep(5000);
				if (LogOut.contains("Logout") && MyAcc.contains("My ACC")) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			// verifyLoginForCVQuality(reportPath, dRDriver, logger,
			// reportingPath, et);
			// ScenarioForAcc.page_Wait();
			// ScenarioForAcc.page_Wait();

			dRDriver.get("http://cvquality.acc.org/Login");
			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			try {
				String LogOutLink = dRDriver
						.findElement(
								By.xpath(".//*[@id='TopNavigation_T254828A4001_ctl00_ctl00_navigationUl']/li[5]/a"))
						.getText();
				Thread.sleep(5000);
				Thread.sleep(5000);
				if (LogOutLink.contains("LOGOUT")) {
					logger.log("pass", "CV Quality page displayed", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "CV Quality page displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "CV Quality page displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			dRDriver.get("http://author.education.acc.org/sitecore/login");

			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			dRDriver.get("http://cvquality.acc.org/Login");
			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}
			try {
				String LogOutLink = dRDriver
						.findElement(
								By.xpath(".//*[@id='TopNavigation_T254828A4001_ctl00_ctl00_navigationUl']/li[5]/a"))
						.getText();
				Thread.sleep(5000);
				Thread.sleep(5000);
				if (LogOutLink.contains("LOGOUT")) {
					logger.log(
							"pass",
							"CV Quality page displayed and loggin session continues after navigatng to other website",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "CV Quality page not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "CV Quality page not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
			// /////////////////
		
			Set<Cookie> allCookies4 = null;

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie"))
				{
				allCookies4 = dRDriver.manage().getCookies();
			}

			// Set<Cookie> allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
			// dRDriver = OpenChrmBrowser(dRDriver, capabilities);

			if (browserName.equalsIgnoreCase("firefox")) {
				dRDriver = OpenFirefoxBrowser(dRDriver);
			} else if (browserName.equalsIgnoreCase("chrome")) {
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else {
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}

			dRDriver.get("http://cvquality.acc.org/Login");
//			dRDriver.manage().window().maximize();

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
{
				for (Cookie cookie : allCookies4) {
					try {
						dRDriver.manage().addCookie(cookie);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			// cookieManager();
			/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();*/
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
			{
			dRDriver.navigate().refresh();
			}

			// ///

			try {
				String LogOutLink = dRDriver
						.findElement(
								By.xpath(".//*[@id='TopNavigation_T254828A4001_ctl00_ctl00_navigationUl']/li[5]/a"))
						.getText();

				Thread.sleep(5000);
				if (LogOutLink.contains("LOGOUT")) {
					logger.log(
							"pass",
							"CV Quality page displayed and loggin session continues after closing and reopening the browser",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "CV Quality page displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "CV Quality page displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			

			logger.flush();
			dRDriver.get("http://www.acc.org");
			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			}

			Set<Cookie> allCookies5 = null;

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
				{
				allCookies5 = dRDriver.manage().getCookies();
			}

			// allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
			// dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			// dRDriver = OpenFFBrowser(dRDriver, capabilities);

			if (browserName.equalsIgnoreCase("firefox")) {
				dRDriver = OpenFirefoxBrowser(dRDriver);
			} else if (browserName.equalsIgnoreCase("chrome")) {
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else {
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}

			dRDriver.get("http://www.acc.org");
//			dRDriver.manage().window().maximize();

			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie"))
				{
				for (Cookie cookie : allCookies5) {
					try {
						dRDriver.manage().addCookie(cookie);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			// cookieManager();
			/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();*/
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")|| browserName.equalsIgnoreCase("ie")) 
			{
				dRDriver.navigate().refresh();
			}

			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log(
							"pass",
							"Login session exits after closing and reopening the browser of other website",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			ScenarioForAcc.page_Wait1();

			// VerifyCVQuality(reportPath, dRDriver, logger, reportingPath, et);
			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			verifyingDisclosureLink(reportPath, dRDriver, logger,
					reportingPath, et);
			VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
			// VerifyJACCJournalWithLogin(reportPath, dRDriver, logger,
			// reportingPath, et);
			VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger,
					reportingPath, et);
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static Properties loadProperties() {
		Properties prop = null;
		try {
			prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream stream = loader
					.getResourceAsStream("constants.properties");
			prop.load(stream);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return prop;
	}

	public static void verifyLoginForCVQuality(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {

			dRDriver.get("http://cvquality.acc.org/Login");

			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			try {
				String HomePageLogintab = dRDriver
						.findElement(
								By.xpath(".//*[@id='TopNavigation_T254828A4001_ctl00_ctl00_navigationUl']/li[5]/a"))
						.getText();

				Thread.sleep(5000);
//				Thread.sleep(15000);
				ScenarioForAcc.page_Wait1();
				if (HomePageLogintab.contains("LOGIN")) {
					logger.log("pass", "Home page displayed and " + '"'
							+ HomePageLogintab + '"'
							+ " tab exists ForCVQuality", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "Home page is not displayed ofr CVquality"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e2) {
				logger.log("fail", "Home page is not displayed ofr CVquality"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();
			/*if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) 
			{
			
			ScenarioForAcc.page_Wait1();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			*/

			try {

				dRDriver.findElement(
						By.xpath(".//*[@id='TopNavigation_T254828A4001_ctl00_ctl00_navigationUl']/li[5]/a"))
						.click();
				//ScenarioForAcc.page_Wait();		
			
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{
					Thread.sleep(5000);
//					Thread.sleep(5000);
				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_pnlLogin']/div/h1"))
						.getText();
				Thread.sleep(5000);
//				Thread.sleep(5000);
			//	ScenarioForAcc.page_Wait();
				
				/*if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}*/
				if (Login.contains("Login")) {
					logger.log("pass", "Login page displayed and " + '"'
							+ Login + '"' + " Header exists ForCVQuality",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login page is not displayed ofr CVquality"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e2) {
				logger.log("fail", "Login page is not displayed ofr CVquality"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();

			//ScenarioForAcc.page_Wait();
			
			/*if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}*/
			try {
				/*dRDriver.findElement(
						By.xpath(".//*[@id='content_0_txtUsername']")).click();
				// driver.findElement(By.id("UserName")).clear();
				ScenarioForAcc.page_Wait();*/
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");
				
				dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtUsername']"))
						.sendKeys(UserNameForMembership);
				
				String username = dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtUsername']"))
						.getAttribute("value");
				Thread.sleep(5000);
				System.out.println(username);
				//ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName " + '"' + username + '"'
							+ " successfully entered ForCVQuality", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e2) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			//ScenarioForAcc.page_Wait();

			try {
				/*dRDriver.findElement(
						By.xpath(".//*[@id='content_0_txtPassword']")).click();
				ScenarioForAcc.page_Wait();*/
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");
				
				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtPassword']"))
						.sendKeys(PasswordForMembership);
				
				String password = dRDriver.findElement(
						By.xpath(".//*[@id='ContentPlaceHolder1_C017_txtPassword']"))
						.getAttribute("value");
				Thread.sleep(5000);
				if (!password.isEmpty()) {
					logger.log("pass", "Password " + '"' + password + '"'
							+ " successfully entered ForCVQuality", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e1) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
			// dRDriver.findElement(
			// By.xpath(".//*[@id='content_0_chbxRememberMe']")).click();

			//ScenarioForAcc.page_Wait();

			// page_Wait();

			// CapturingPopup(dRDriver);
			try {
				dRDriver.findElement(By.xpath(".//*[@id='ContentPlaceHolder1_C017_btnLogin']"))
						.click();
				Thread.sleep(15000);
				WebDriverWait wait =new WebDriverWait(dRDriver, 20);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='site-header']/div/div[1]/p/a")));
					
				/*Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);*/
			//	ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{
					Thread.sleep(5000);
				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
				// et=logger.createTest("Login", "Login successful");

				System.out
						.println("Successfully logged in to CV Quality site ForCVQuality");
				String MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='site-header']/div/div[1]/p/a")).getText();
				Thread.sleep(5000);
			
				String LogOut = dRDriver.findElement(
						By.xpath(".//*[@id='qiiLoginStatus_lnkLogout']")).getText();
				Thread.sleep(5000);
				/*Thread.sleep(5000);
				Thread.sleep(5000);*/
				
				if (LogOut.contains("Logout") && MyAcc.contains("My ACC")) {
					logger.log("pass",
							"Successfully logged into ACC ForCVQuality", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void verifyLoginForACCScientific(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {

			dRDriver.get("https://accscientificsession.acc.org/");

			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			List<WebElement> loginPagePopUP = dRDriver.findElements(By
					.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
			//ScenarioForAcc.page_Wait();
			
			/*if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}*/
			if (loginPagePopUP.size() > 0) {
				loginPagePopUP.get(0).click();
				//ScenarioForAcc.page_Wait();				
				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{

				ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				}
			}

			/**/try {
				String LoginPage = dRDriver.findElement(
						By.xpath(".//*[@id='login_btn']")).getText();
				Thread.sleep(5000);
			//	ScenarioForAcc.page_Wait();
				if (LoginPage.contains("Login")) {
					logger.log("pass",
							"Login page for ACCScentific is displayed and  "
									+ '"' + LoginPage + '"'
									+ " tab exixts ForACCScientific", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Login page for ACCScientific page is not displayed"
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e2) {
				logger.log("fail",
						"Login page for ACCScientific page is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();

		//	ScenarioForAcc.page_Wait();

			try {

				/*dRDriver.findElement(By.xpath(".//*[@id='username']")).click();
				ScenarioForAcc.page_Wait();*/
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath(".//*[@id='username']"))
						.sendKeys(UserNameForMembership);
			
				String username = dRDriver.findElement(
						By.xpath(".//*[@id='username']")).getAttribute("value");
				Thread.sleep(5000);
				System.out.println(username);
				ScenarioForAcc.page_Wait1();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName " + '"' + username + '"'
							+ " successfully entered ForACCScientific", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e2) {
			
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			//ScenarioForAcc.page_Wait();

			try {

				/*dRDriver.findElement(By.xpath(".//*[@id='password']")).click();
				ScenarioForAcc.page_Wait();*/
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.xpath(".//*[@id='password']"))
						.sendKeys(PasswordForMembership);
				
				String password = dRDriver.findElement(
						By.xpath(".//*[@id='password']")).getAttribute("value");
				Thread.sleep(5000);
				if (!password.isEmpty()) {
					logger.log("pass", "Password " + '"' + password + '"'
							+ "successfully entered ForACCScientific", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e1) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			//ScenarioForAcc.page_Wait();

			// page_Wait();

			// CapturingPopup(dRDriver);
			try {

				dRDriver.findElement(By.xpath(".//*[@id='login_btn']")).click();
				Thread.sleep(25000);
				/*Thread.sleep(5000);
				Thread.sleep(5000);
				Thread.sleep(5000);*/
				//ScenarioForAcc.page_Wait();
				//WebDriverWait wait = new WebDriverWait(dRDriver, 20);
				//wait.until(ExpectedConditions.elementToBeClickable(By
					//	.xpath(".//*[@id='logout_btn']")));

				if (browserName.equalsIgnoreCase("firefox")
									|| browserName.equalsIgnoreCase("chrome")) 
				{
//					WebDriverWait wait = new WebDriverWait(dRDriver, 20);
					element=dRDriver.findElement(By.xpath(".//*[@id='logout_btn']"));
				//ScenarioForAcc.page_Wait();
					
					waitForURLToMatch(element,20);
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
				}
				
				
				// et=logger.createTest("Login", "Login successful");

				System.out.println("Successfully logged in to acc scientific");

				String MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='login_link']")).getText();
				Thread.sleep(5000);
				
				String LogOut = dRDriver.findElement(
						By.xpath(".//*[@id='logout_btn']")).getText();
				Thread.sleep(5000);
			
				if (LogOut.contains("Logout") && MyAcc.contains("My ACC")) {
					logger.log("pass",
							"Successfully logged into ACC ForACCScientific",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			//ScenarioForAcc.page_Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void verifyLoginForJACCJournal(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			dRDriver.get("http://onlinejacc.org/journals.aspx");
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) 
			{
				ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
			}
			try {
				String HomepageforLogin = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignIn']"))
						.getText();

				//ScenarioForAcc.page_Wait();
				if (HomepageforLogin.contains("LOGIN")) {
					logger.log("pass",
							"Home page for JACCJpurnal is displayed and  "
									+ '"' + HomepageforLogin + '"'
									+ " tab exixts ForJACCJournal", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Home page for JACCJournal page is not displayed"
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e2) {
				logger.log("fail",
						"Home page for JACCJournal page is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();

			//ScenarioForAcc.page_Wait();

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignIn']")).click();
				//ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{
					ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
				}
				String LoginPage = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

				//ScenarioForAcc.page_Wait();
				if (LoginPage.contains("Login")) {
					logger.log("pass",
							"Login page for JACCJournal is displayed and  "
									+ '"' + LoginPage + '"'
									+ " Header exixts For JACCJournal", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Login page for JACCJournal page is not displayed"
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e2) {
				logger.log("fail",
						"Login page for JACCJournal page is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();

			//ScenarioForAcc.page_Wait();

			try {
				/*dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
				// driver.findElement(By.id("UserName")).clear();
				ScenarioForAcc.page_Wait();*/
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
						UserNameForMembership);
				String username = dRDriver.findElement(
						By.xpath("//*[@id='UserName']")).getAttribute("value");

				System.out.println(username);
				//ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName " + '"' + username + '"'
							+ " successfully entered", false, dRDriver,
							reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

		//	ScenarioForAcc.page_Wait();

			try {

				/*dRDriver.findElement(By.id("Password")).click();
				ScenarioForAcc.page_Wait();*/
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				dRDriver.findElement(By.id("Password")).sendKeys(
						PasswordForMembership);
				String password = dRDriver.findElement(By.id("Password"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password " + '"' + password + '"'
							+ " successfully entered ForJACCJournal", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
			 * .click();
			 */

			//ScenarioForAcc.page_Wait();

			try {
				dRDriver.findElement(By.xpath(".//*[@id='formLogin']/input[3]"))
						.click();
				//ScenarioForAcc.page_Wait();
				
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{
					ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
				}
				// et=logger.createTest("Login", "Login successful");

				System.out.println("Successfully logged in to ForJACCJournal");

				// page_Wait();

				CapturingPopup(dRDriver);

				WebElement LogoForJACC = dRDriver.findElement(By
						.xpath(".//*[@id='siteLogo']"));

				if (LogoForJACC.isDisplayed()) {
					logger.log("pass",
							"Logged in successfully  ForJACCJournal", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Logged in not uccessfull  ForJACCJournal"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail", "Logged in not uccessfull  ForJACCJournal"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();
			//ScenarioForAcc.page_Wait();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void verifyLoginForACCSAP9(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) 
			{
				ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
			}
			try {
				String HomepageforLogin = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();

				ScenarioForAcc.page_Wait();
				Thread.sleep(5000);
				if (HomepageforLogin.contains("Log in to MyACC")) {
					logger.log("pass",
							"Home page for ACCSAP9 is displayed and  " + '"'
									+ HomepageforLogin + '"'
									+ " tab exixts For ACCSAP9", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Home page for ACCSAP9 page is not displayed" + "\""
									+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e2) {
				logger.log("fail",
						"Home page for ACCSAP9 page is not displayed" + "\""
								+ "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();

			ScenarioForAcc.page_Wait();
			
		ScenarioForAcc.page_Wait();

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.click();
				ScenarioForAcc.page_Wait();
				Thread.sleep(15000);
				
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{
					ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
				}
				CapturingPopUp(dRDriver, browserName);

				String LoginPage = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				Thread.sleep(5000);
				ScenarioForAcc.page_Wait();
				if (LoginPage.contains("Login")) {
					logger.log("pass",
							"Login page for ACCSAP9 is displayed and  " + '"'
									+ LoginPage + '"'
									+ " header exixts For ACCSAP9", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail",
							"Login page for ACCSAP9 page is not displayed" + "\""
									+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e2) {
				logger.log("fail",
						"Login page for ACCSAP9 page is not displayed" + "\""
								+ "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();

			//ScenarioForAcc.page_Wait();

			try {

				//dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
				// driver.findElement(By.id("UserName")).clear();
				//ScenarioForAcc.page_Wait();
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils
						.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
						UserNameForMembership);
				String username = dRDriver.findElement(
						By.xpath("//*[@id='UserName']")).getAttribute("value");

				System.out.println(username);
				//ScenarioForAcc.page_Wait();

				if (!username.isEmpty()) {
					logger.log("pass", "UserName " + '"' + username + '"'
							+ " successfully entered ForACCSAP9", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			//ScenarioForAcc.page_Wait();

			try {

				//dRDriver.findElement(By.id("Password")).click();
				//ScenarioForAcc.page_Wait();
				String PasswordForMembership = Utils
						.loadProperty("PasswordForMembership");

				// driver.findElement(By.id("Password")).sendKeys(vPassword);

				dRDriver.findElement(By.id("Password")).sendKeys(
						PasswordForMembership);
				String password = dRDriver.findElement(By.id("Password"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password " + '"' + password + '"'
							+ " successfully entered ForACCSAP9", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			} catch (Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
			 * .click();
			 */

			//ScenarioForAcc.page_Wait();

			try {

				dRDriver.findElement(By.xpath(".//*[@id='formLogin']/div[3]/button"))
						.click();

				Thread.sleep(15000);
				try {
					List<WebElement> loginPagePopUP = dRDriver.findElements(By
							.xpath(".//*[@id='acsMainInvite']/div/a[1]"));
					//ScenarioForAcc.page_Wait();
					
					if (browserName.equalsIgnoreCase("firefox")
										|| browserName.equalsIgnoreCase("chrome")) 
					{

					ScenarioForAcc.page_Wait();
					}
					else if(browserName.equalsIgnoreCase("ie"))
					{
						dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					}
					if (loginPagePopUP.size() > 0) {
						loginPagePopUP.get(0).click();
					
						if (browserName.equalsIgnoreCase("firefox")
											|| browserName.equalsIgnoreCase("chrome")) 
						{

						ScenarioForAcc.page_Wait();
						}
						else if(browserName.equalsIgnoreCase("ie"))
						{
							dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						}
						//ScenarioForAcc.page_Wait();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// et=logger.createTest("Login", "Login successful");
				//ScenarioForAcc.page_Wait();
//				WebDriverWait wait2 = new WebDriverWait(dRDriver, 20);
				/*element=dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"));

				Thread.sleep(5000);
				boolean ele=waitForURLToMatch(element,20);
				System.out.println(ele);
				if(ele==true)
				{
				System.out
						.println("Successfully logged in to ACC site ForACCSAP9");

				// page_Wait();

				CapturingPopup(dRDriver);
			//	ScenarioForAcc.page_Wait();
				}*/
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome")) 
				{
					ScenarioForAcc.page_Wait();
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
				}
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				Thread.sleep(5000);
				
				
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACCSAP9",
							false, dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Login failed for ACCSAP9" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			} catch (Exception e) {
				logger.log("fail", "Login failed for ACCSAP9" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			//ScenarioForAcc.page_Wait();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void verifyingDisclosureLink(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {
			// newTabOpening(dRDriver);
			try {
				dRDriver.get("http://disclosures.acc.org");
				WebDriverWait wait = new WebDriverWait(dRDriver, 5);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath("html/body/div[1]/div/section/div[1]/div/a")));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				
			//	ScenarioForAcc.page_Wait();
				Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
				String browserName = cap.getBrowserName().toLowerCase();
				if (browserName.equalsIgnoreCase("firefox")
						|| browserName.equalsIgnoreCase("chrome"))
				{
					ScenarioForAcc.page_Wait();
					Thread.sleep(5000);
					/*WebElement element = dRDriver.findElement(
							By.xpath("html/body/div[1]/div/section/div[1]/div/a"));
					waitForURLToMatch(element,30);*/
					WebDriverWait wait = new WebDriverWait(dRDriver, 5);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By
							.xpath("html/body/div[1]/div/section/div[1]/div/a")));
				}
				else if(browserName.equalsIgnoreCase("ie"))
				{
					dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
				}
				logger.log("pass", "Navigated to disclosure page", false,
						dRDriver, reportingPath, et);
			} catch (Exception e) {
				logger.log("fail", " does not Navigated to disclosure page ", true,
						dRDriver, reportingPath, et);
			}
			logger.flush();
			try {
				String ManageYourDisclosures = dRDriver.findElement(
						By.xpath("html/body/div[1]/div/section/div[1]/div/a"))
						.getText();

			/*	String MergeAccounts = dRDriver.findElement(
						By.xpath("html/body/div[1]/div/section/div[2]/div/a"))
						.getText();*/

				String Logout = dRDriver.findElement(
						By.xpath("html/body/header/div/div[2]/span/a[2]/span"))
						.getText();

				if (ManageYourDisclosures.contains("Manage Your Disclosures")
						/*&& MergeAccounts.contains("Merge Accounts")*/
						&& Logout.contains("Logout")) {
					logger.log("pass",
							"Navigated to annual disclosure page and " + '"'
									+ Logout + '"' + " "
									+ ManageYourDisclosures + '"' + " " + '"'
									+ '"' + " exists", false,
							dRDriver, reportingPath, et);
				}
				else
				{
					logger.log("fail", "Navigation to annual diclosure page failed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail", "Navigation to annual diclosure page failed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();
			// ScenarioForAcc.page_Wait();

		} catch (Exception e) {
			e.printStackTrace();
		}
		// newTabClosing(dRDriver);
	}

	public static void newTabOpening(WebDriver dRDriver)
			throws Exception {
		/*
		 * driverFF=dRDriver; driverChrome=dRDriver;
		 */

		/*
		 * WebElement body = dRDriver.findElement(By.tagName("body"));
		 * body.sendKeys(Keys.CONTROL + "t");
		 */
		Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();

		if (browserName.equalsIgnoreCase("firefox")) {
			WebElement body = dRDriver.findElement(By.tagName("body"));
			body.sendKeys(Keys.CONTROL + "t");
			ArrayList<String> tabs2 = new ArrayList<String>(
					dRDriver.getWindowHandles());
			System.out
					.println("Firefox##################################Number of windows opened are : "
							+ tabs2.size());
			//System.out.println(dRDriver.getCurrentUrl());
			// dRDriver.switchTo().window(tabs2.get(1));
		} else if (browserName.equalsIgnoreCase("chrome")) {
			// String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,"t");
			WebElement body = dRDriver.findElement(By.tagName("body"));
			// body.sendKeys(selectLinkOpeninNewTab);
			body.sendKeys(Keys.CONTROL + "t");
			ArrayList<String> tabs2 = new ArrayList<String>(
					dRDriver.getWindowHandles());
			System.out
					.println("Chrome##################################Number of windows opened are : "
							+ tabs2.size());
			//System.out.println(dRDriver.getCurrentUrl());
			/*Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();*/
			if (browserName.equalsIgnoreCase("firefox")
								|| browserName.equalsIgnoreCase("chrome")) 
			{

			ScenarioForAcc.page_Wait1();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}
			dRDriver.switchTo().window(tabs2.get(1));
		}
	}

	public static void newTabClosing(WebDriver dRDriver) {
		/*
		 * driverFF=dRDriver; driverChrome=dRDriver;
		 */
		Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();

		if (browserName.equalsIgnoreCase("firefox")) {
			System.out.println("in ff closing tab");
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			System.out.println("tab is closed");
			dRDriver.switchTo().defaultContent();

		} else if (browserName.equalsIgnoreCase("chrome")) {
			ArrayList<String> tabs2 = new ArrayList<String>(
					dRDriver.getWindowHandles());
			dRDriver.close();
			dRDriver.switchTo().window(tabs2.get(0));
		}
	}

	public static void LoggerForHomepageDisplayWithLogin(
			com.gallop.Logger logger, WebDriver dRDriver, ExtentTest et)
			throws Exception {
		try {
			//ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) 
			{
				ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
			}
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			//System.out.println(dRDriver.getCurrentUrl());
			System.out.println("Entered Logger For HomePage Display");
			List<WebElement> HomePage1 = dRDriver
					.findElements(By
							.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_rptMyAcc_liHeader_0']"));
			List<WebElement> HomePage2 = dRDriver.findElements(By
					.xpath(".//*[@id='nav-myacc']/li[1]/span"));// /////////////////////////////////
			// .//*[@id='nav-myacc']/li[1]/span

			List<WebElement> HomePage3 = dRDriver.findElements(By
					.xpath(".//*[@id='msTopNav_rptNav_hypLnk_0']"));

			//ScenarioForAcc.page_Wait();
			
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) 
			{
				ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
			}
			// dRDriver.manage().
			if (HomePage1.get(0).isDisplayed()
					|| HomePage2.get(0).isDisplayed()
					|| HomePage3.get(0).isDisplayed()) {
				System.out.println("Logging true");
				logger.log("pass", "Home page displayed for acc", false,
						dRDriver, reportingPath, et);
			} else {
				System.out.println("Failed to display homepage");
				logger.log("fail", "Home page not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {

			logger.log("fail", "Home page not displayed" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath, et);
		}
		logger.flush();
		//ScenarioForAcc.page_Wait();
	}

	public static void redirectToHomePage(com.gallop.Logger logger,
			ExtentTest et, WebDriver dRDriver) {

		try {

			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
		//	ScenarioForAcc.page_Wait();
			Capabilities cap = ((RemoteWebDriver) dRDriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			if (browserName.equalsIgnoreCase("firefox")
					|| browserName.equalsIgnoreCase("chrome")) 
			{
				ScenarioForAcc.page_Wait();
			}
			else if(browserName.equalsIgnoreCase("ie"))
			{
				dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
			}
			WebElement HomePage = dRDriver.findElement(By
					.xpath(".//*[@id='nav-primary-holder']/div[3]"));
			// .//*[@id='nav-myacc']/li[1]/span

			if (HomePage.isDisplayed()) {
				System.out.println("Logging true");
				logger.log("pass", "Home page displayed for acc", false,
						dRDriver, reportingPath, et);
			} else {
				System.out.println("Failed to display homepage");
				logger.log("fail",
						"Updated Date and Original POsted date are not same "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}
			/*
			 * logger.log("pass",
			 * "Updated Date and Original POsted date are same ", false,
			 * dRDriver, reportingPath, et);
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();

			logger.log("fail",
					"Updated Date and Original POsted date are not same "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
					reportingPath, et);
		}

		logger.flush();
	}

	public static void AlertForFederatedLogin() {
		try {
			dRDriver.switchTo().alert().accept();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
static boolean truefalse;
static WebElement currentURL;
	public static boolean waitForURLToMatch(final WebElement element,int waitTime){
	    Wait<WebDriver> wait = new WebDriverWait(dRDriver, 20);
	    
	    Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>() {
	        public Boolean apply(WebDriver driver) {
//	            currentURL = dRDriver.findElement((By.xpath(".//*[@id='logout_btn']")));
	            if(element.isDisplayed())
	            {
	                truefalse = true;
	                return truefalse;
	            }
	            truefalse = false;
	            return truefalse;
	        }
	    };
	    try{
	        wait.until(function);
	    } catch (Exception e){   
	    }
	    return truefalse;
	}
	
}
