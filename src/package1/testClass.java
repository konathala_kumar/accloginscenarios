package package1;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.gallop.acc.org.ScenarioForAcc;
import com.relevantcodes.extentreports.ExtentTest;

import utility.Utils;

public class testClass {
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");
	private static String ieDriver = Utils.loadProperty("ieDriver");
	
	//static 
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		com.gallop.Logger logger = new com.gallop.Logger();
		//logger.createTestReport(reportPath + "\\testReports.html"/* "Q:\\QA Stuff\\Syam pithani\\NCDR\\NCDR\\WebContent\\WEB-INF\\views\\testReports.html" */);

		ExtentTest et = null;
		System.setProperty("webdriver.ie.driver", /* Constant.Path_TestData */
				ieDriver + "IEDriverServer.exe");
		WebDriver dRDriver = new InternetExplorerDriver();
		dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
		dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//LoginBehaviourofACC.CapturingPopup(dRDriver);
		WebElement action1 = dRDriver.findElement(By
				.cssSelector(".header-link.header-myacc>p>a")); // login
																	// button
																	// validation
		if (action1.isDisplayed()) {
			System.out.println("Element displayed");
			action1.click();
			dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);}
		String UserNameForMembership = Utils
				.loadProperty("UserNameForMembership");

		dRDriver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys(
				UserNameForMembership); // this enters the username
		String PasswordForMembership = Utils
				.loadProperty("PasswordForMembership");

		// driver.findElement(By.id("Password")).sendKeys(vPassword);

		dRDriver.findElement(By.id("Password")).sendKeys(
				PasswordForMembership); // this will enter the password
										// data
		
		WebElement signIn = dRDriver.findElement(By
				.xpath(".//*[@id='formLogin']/button"));
		// check remember me check
		
		if (signIn.isDisplayed()) {
			signIn.click();
			dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		LoginBehaviourofACC.CapturingPopup(dRDriver);
		Set<Cookie> allCookies = null;
		allCookies = dRDriver.manage().getCookies();
		dRDriver.close();
		 dRDriver = new InternetExplorerDriver();
		dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
		dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		for (Cookie cookie : allCookies) {
			try {
				dRDriver.manage().addCookie(cookie);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		dRDriver.navigate().refresh();
		WebElement ele = dRDriver.findElement(By.xpath("//a[contains(text(), 'Log Out')]"));
		System.out.println("this is for testing the logout text " + ele.getText());
		if(ele.isDisplayed())
		{
			ele.click();
			ScenarioForAcc.page_Wait();
			}
	}

}
